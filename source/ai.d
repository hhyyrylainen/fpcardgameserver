
import match;
import player;
import cards;

import std.stdio;
import core.time;
import std.conv;
import std.datetime;
import std.random;
import std.algorithm;
import std.array;

class AIDecisionMaker {

    template AiHelper(string T){

        enum string DataCreateAndStartCheck = "
            if(ai.Data is null){

                ai.Data = new " ~ T ~ ";
            }

            ai.Data.ResetNewTurn();
            if(ai.Data.TurnNumber != match.TurnNumber){

                ai.Data.StartNewTurn(match.TurnNumber, totalelapsed);
            }
            " ~ T ~ " data = cast(" ~ T ~ ")(ai.Data);
            ";
    }

    //! Performs an AI turn
    public static bool PlayTurn(AI ai, Match match, Participant part,
        Duration timesinceturnstart)
    {

        switch(ai.Type){
        case AI_TYPE.EASY:
            return PlayEasy(ai, match, part, timesinceturnstart);
        default:
            throw new Exception("AI Type: unimplemented actual action taking");
        }
    }

    protected static bool PlayEasy(AI ai, Match match, Participant part,
        Duration totalelapsed)
    {
        mixin(AiHelper!("EasyAIData").DataCreateAndStartCheck);


        // Basic gameplay: play all possible cards -> attack with cards randomly ->
        // play a post if possible -> end turn

        bool playedcard = false;

        for(ulong i = 0; i < part.CountCardsInHand; ++i){

            auto card = part.GetCard(i);

            if(match.CanPlayCard(part, card)){

                bool played = false;
                
                // Spell cards need a target //
                if(card.Type == CardType.spell){

                    if(!card.Effect.RequiresTarget){

                        match.PlayCard(card, part);
                        played = true;
                        
                    } else {

                        // Find a target //
                        auto validtargets = match.FindTargetsForSpell(part, card.Effect);

                        if(validtargets.count > 0){
                            
                            auto targetcard = validtargets[uniform(0, validtargets.length)];

                            match.PlayCard(card, part);
                            match.AIApplyTargetSelect(part, targetcard);
                            played = true;
                        }
                    }
                    
                } else if(card.Type == CardType.user){
                
                    match.PlayCard(card, part);
                    played = true;
                }

                if(played){
                    ai.Data.DidAction(totalelapsed);
                    playedcard = true;
                }
            }
        }

        // Attack with all cards once has played all cards //
        if(!playedcard){

            for(ulong i = 0; i < part.CountCardsOnField; ++i){

                auto card = part.GetFieldedCard(i);

                if(!card.AttackedThisTurn){

                    // Check can attack anything //
                    auto targets = match.FindTargetsCardCanAttack(part, card);

                    if(targets.length < 1)
                        continue;

                    auto targetcard = targets[uniform(0, targets.length)];
                    
                    // Do the attack //
                    match.AttackCard(part, card, targetcard);
                    ai.Data.DidAction(totalelapsed);
                    
                    break;
                }
            }
            
        }

        // Check posting //
        if(!part.HasPosted && (totalelapsed - ai.Data.IdleTime >= seconds(1))){

            // Check can the ai post //
            auto const oldCount = part.PossiblePosts.length;

            assert(oldCount > 0);
                
            foreach(post; part.PossiblePosts){

                bool posted = false;
                    
                if(!match.AIDoPost(part, post, post.PostData.Effect)){

                    // Needs to choose a target //
                    auto possibletargets = match.FindTargetsForPost(part, post.PostData.Effect);

                    DrawnCard target = null;

                    if(post.PostData.Effect.AIShouldTargetOwn){

                        // Select among own cards //
                        auto owncards = array(possibletargets.filter!(
                                c => c.WhoseCard == part.PlayerNumber));

                        if(owncards.length > 0)
                            target = owncards[uniform(0, owncards.length)];
                            
                    } else if(post.PostData.Effect.AIShouldTargetEnemy){

                        // Select from enemy cards //
                        auto enemycards = array(possibletargets.filter!(
                                c => c.WhoseCard != part.PlayerNumber));

                        if(enemycards.length > 0)
                            target = enemycards[uniform(0, enemycards.length)];
                    }
                        
                    if(target is null && possibletargets.length > 0){

                        // Failed to pick a good target, just randomize it //
                        target = possibletargets[uniform(0, possibletargets.length)];
                    }

                    // Now try again with a proper target //
                    if(match.AIDoPost(part, post, post.PostData.Effect, target))
                        posted = true;
                    else
                        writeln("AI failed to find a target for targeted post");
                        
                } else {

                    // Successfully posted //
                    posted = true;
                }

                if(posted){

                    ai.Data.DidAction(totalelapsed);
                    assert(oldCount != part.PossiblePosts.length);
                    return false;                        
                }
            }
        }
        
        // Check turn end and post//
        if(totalelapsed - ai.Data.IdleTime >= seconds(2)){

            return true;
        }

        // Not ready yet //
        return false;
    }

    unittest{

        import games;

        // Testing AI //

        // Easy //
        auto easyai = new AI(AI_TYPE.EASY);

        auto manager = new GameManager;

        auto match = new Match(easyai, easyai, manager);

        match.State = MATCH_STATE.TURN_PLY1;
        auto part = match.CurrentPlayer;

        // TODO: force a specific card int othe AI's hand for testing
        // The AI got a single card when its turn began
        //match.DrawCards(part, 1);

        assert(PlayTurn(easyai, match, part, seconds(1)) == false);

        // Check that AI played the card //
        //assert(part.CountCardsInHand == 0);
    }


    unittest{

        import games;

        // Testing AI doing posts //

        auto easyai = new AI(AI_TYPE.EASY);

        auto manager = new GameManager;

        auto match = new Match(easyai, easyai, manager);

        match.State = MATCH_STATE.TURN_PLY1;
        auto part = match.CurrentPlayer;

        assert(part.PossiblePosts.length > 0);

        // Should play a card here
        assert(part.CountCardsInHand == 1);
        assert(PlayTurn(easyai, match, part, seconds(1)) == false);
        // TODO: fix this to get specific cards
        //assert(part.CountCardsInHand == 0);

        // TODO: fix this when it is possible to get only targeted posts
        if(!PlayTurn(easyai, match, part, seconds(3))){

            assert(PlayTurn(easyai, match, part, seconds(6)) == true);            
        }
    }
}

//! Contains data the AI needs during a single turn, or a match
class AIData {


    public void StartNewTurn(int turnnumber, Duration start){

        TurnNumber = turnnumber;
        TurnStart = start;
        IdleTime = TurnStart;

        TurnBegan = true;
    }

    public void ResetNewTurn(){

        TurnBegan = false;
    }

    public void DidAction(Duration attime){

        IdleTime = attime;
    }

    Duration TurnStart;
    //! Used to end AI turns 
    Duration IdleTime;
    int TurnNumber = -1;

    bool TurnBegan = false;
}

class EasyAIData : AIData {

    
}
