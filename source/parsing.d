import std.string;
import std.conv : to;
import std.exception;

class EffectParser {

public:
    //! Parses an effect multiplier of the form 'x{number}'
    static int ParseMultiplier(in string str){

        enforce(str.length >= 2);

        if(str[0] != 'x'){

            throw new Exception("multiplier doesn't start with 'x'");
        }
        
        return to!int(str[1..$]);
    }

    unittest{

        assert(ParseMultiplier("x3") == 3);
        assert(ParseMultiplier("x8") == 8);
        assert(ParseMultiplier("x-8") == -8);
    }
}
