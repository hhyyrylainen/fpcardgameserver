// Module containing the game manager and login handler //
import player;
import connection;
import match;
import cards;
import utility;
import messages;
import threads;

import std.exception;
import std.random;
import core.sync.mutex;
import std.algorithm;
import std.typecons : tuple, Tuple;
//import std.concurrency; // Don't use things that totally block vibe.d

import vibe.d;
import vibe.core.core;
//import vibe.core.concurrency; // uses std.concurrency instead

static immutable Duration GAME_TICK_INTERVAL = 500.msecs;

class TaskWrapper {

    public this(Task wrap){

        task = wrap;
    }
    
    public Task task;
}

//! Handles matches and logins, only one should exist per server instance to allow online players
//! to see each other
class GameManager_ {

    public this(){

        //MatchMutex = cast(shared)(new Mutex);
        GameCards = new CardManager;
        GameThreads = new ThreadManager;

        MatchTask = new TaskWrapper(runTask({ MatchStatusUpdater; }));
        LocalControlTask = new TaskWrapper(runTask({ ProcessUnixCommandSignals; }));

        
    }

    //! Thread safe function to process localhost rcon command
    public shared void ProcessLocalCommand(in string cmd){

        if(LocalControlTask is null)
            return;

        logDebug("Command '" ~ cmd ~ "' queued");
        LocalControlTask.task.sendCompat(cmd);
    }

    //! Called when a player logs in
    public void RegisterPlayer(Player newplayer){

        assert(newplayer.SentCards.SentCount == 0);

        // Check that name is not in use / reserved //
        foreach(player; OnlinePlayers){

            if(newplayer.Name == player.Name){

                throw new Exception(newplayer.Name ~ " is already in use");
            }
        }

        OnlinePlayers ~= newplayer;

        // Login succeeded //
        
        // Tell player that they have logged in before we send all sorts of stuff //
        newplayer.SendMessage(new HelloMessage(newplayer.Name));

        BroadcastMessage("'" ~ newplayer.Name ~ "' Connected");
        
        // Send the new player the list of current players //
        Tuple!(string, bool)[] playernames;

        foreach(player; OnlinePlayers){

            playernames ~= tuple(player.Name, player.AcceptingChallengeRequests);
        }
        
        newplayer.SendMessage(new ClientListMessage(playernames));

        BroadcastOnline(newplayer, true);
    }

    //! Called when a player logs out
    public void LogOut(Player leavingplayer){

        for(ulong i = 0; i < OnlinePlayers.count; ++i){

            if(OnlinePlayers[i] == leavingplayer){

                BroadcastOnline(leavingplayer, false);
                OnlinePlayers = OnlinePlayers.remove!(SwapStrategy.unstable)(i);
                break;
            }
        }

        // End matches this player is part of //
        EndPlayerMatches(leavingplayer);

        BroadcastMessage("Player '" ~ leavingplayer.Name ~ "' Left");

        // Make sure that no one is waiting for a challenge with this player //
        EndChallengesForTarget(leavingplayer);
    }

    //! Ends challenges for player
    public void EndChallengesForTarget(Player target){

        // Cancel their own challenge first //
        auto targetschallenge = GetPlayer(target.ChallengedPlayer);

        if(targetschallenge !is null){

            targetschallenge.SendMessage(new NotifyChallengedMessage(target.Name, false));
            target.SendMessage(new NotifyChallengedMessage(target.ChallengedPlayer, false));
        }

        target.ChallengedPlayer = "";
        
        foreach(ply; OnlinePlayers){

            if(ply.ChallengedPlayer == target.Name){

                logInfo("Canceling challenge as the challenged player is no longer eligible");

                ply.SendMessage(new ChallengeStatusMessage(target.Name, false,
                        "Opponent disconnected / entered a match"));
                ply.ChallengedPlayer = "";
            }
        }
    }

    //! Called when a player cancels a request or denies a request
    public void ChallengeCancelled(Player ply, in string opponent){

        logInfo(ply.Name ~ " canceled challenging: " ~ opponent);
        
        auto target = GetPlayer(opponent);
        
        if(target !is null && ply.Name == target.ChallengedPlayer){

            // Denied request //
            target.SendMessage(new ChallengeStatusMessage(ply.Name, false,
                                                          "Opponent denied the request"));
            target.ChallengedPlayer = "";
            return;
        }

        // Notify cancelled //
        if(opponent == ply.ChallengedPlayer && target !is null)
            target.SendMessage(new NotifyChallengedMessage(ply.Name, false));

        // Cancelled request //
        ply.ChallengedPlayer = "";
    }

    //! A player challenges another
    //! Sends notifications to both players
    public void ChallengePlayer(Player challenger, Player target)
        in{
            assert(target !is null);
            assert(challenger !is null);
        }
    body{

        if(challenger is target)
            return;

        // Detect if the challenger is responding to an earlier request //
        if(challenger.Name == target.ChallengedPlayer){

            target.SendMessage(new ChallengeStatusMessage(challenger.Name, true, ""));
            
            challenger.SendMessage(new ChallengeStatusMessage(target.Name, true, ""));

            logInfo("Player Challenger accepted, match started!");
            
            StartMatch(target, challenger);
            return;
        }

        // Disallow if target is not accepting challenges //
        if(!target.AcceptingChallengeRequests){

            challenger.SendMessage(new ChallengeStatusMessage(target.Name, false,
                    "Opponent not accepting requests"));
            challenger.ChallengedPlayer = "";
            return;
        }
        
        // Notify target //
        challenger.ChallengedPlayer = target.Name;
        target.SendMessage(new NotifyChallengedMessage(challenger.Name, true));
    }

    public Match StartMatch(Player first, Player second){

        logInfo("[MATCH] " ~ first.Name ~ " vs. " ~ second.Name);
        
        // Make sure player is not a part of any matches //
        EndPlayerMatches(first);
        EndPlayerMatches(second);

        // Disallow challenges while match is active //
        PlayerEnterMatchNoChallengesAccepted(first);
        PlayerEnterMatchNoChallengesAccepted(second);

        // Deny all active challenges //
        EndChallengesForTarget(first);
        EndChallengesForTarget(second);

        // Randomize who goes first
        Match match = uniform(0, 2) == 0 ? new Match(first, second, this) :
            new Match(second, first, this);
        
        OngoingGames ~= match;

        first.SendMessage(new MatchStartMessage(second.Name));
        second.SendMessage(new MatchStartMessage(first.Name));

        return match;
    }

    // Starts a single player match
    public Match StartAIMatch(Player ply, const AI_TYPE type){

        enforce(type != AI_TYPE.PLAYER);

        // Make sure player is not a part of any matches //
        EndPlayerMatches(ply);

        // Deny all active challenges //
        EndChallengesForTarget(ply);
        
        auto aiinstance = new AI(type);

        auto match = new Match(ply, aiinstance, this);
        
        OngoingGames ~= match;
        
        return match;
    }

    //! Ends all matches forplayer is part of
    public void EndPlayerMatches(Player forplayer){

        if(forplayer.PartInMatch !is null){

            forplayer.PartInMatch.PlayerLeft(forplayer);
        }

        // A backup method //
        // foreach(Match match; OngoingGames){
            
        //     match.PossiblyRelevantPlayerLeft(forplayer);
        // }

        assert(forplayer.PartInMatch is null);
    }

    //! Player sends a chat message
    public void Chat(Player ply, ChatMessageMessage message){

        enforce(message.Message.length <= 400);
        enforce(message.Sender.length <= 100);

        if(message.Message.length < 1)
            return;
        
        if(ply.RateLimitChat){

            ply.SendMessage(new ChatMessageMessage("You are sending too many chat messages, " ~
                    "slow down!"));
            logInfo("[CHAT RATE LIMIT] hit by '" ~ message.Sender ~ "'");
            return;
        }

        logInfo("[CHAT] " ~ message.Sender ~ ": " ~ message.Message);

        SendMessageAll(message, true);
    }

    //! Player is not receiving challenges
    public void PlayerEnterMatchNoChallengesAccepted(Player ply){

        ply.AcceptingChallengeRequests = false;
        BroadcastChallengeable(ply, false);
    }

    //! Called from Match when a player is removed. After this the player can now
    //! receive challenges
    public void PlayerExitMatchAllowChallenges(Player ply){

        if(ply is null)
            return;
        
        ply.AcceptingChallengeRequests = true;
        BroadcastChallengeable(ply, true);
    }
    
    //! Broadcasts a server message to all online players
    public void BroadcastMessage(in string message){

        logInfo("[Broadcast]: " ~ message);
        SendMessageAll(new ChatMessageMessage(message), true);
    }

    //! Sends a message to all players
    public void SendMessageAll(IMessage message, bool skipinmatch = false){

        foreach(ply; OnlinePlayers){

            if(skipinmatch && ply.IsInMatch)
                continue;
            
            ply.SendMessage(message);
        }
    }

    //! Broadcasts a message about player being online to other players
    public void BroadcastOnline(Player ply, bool online){

        auto message = new ClientStatusMessage(ply.Name, online);
        
        foreach(notifyply; OnlinePlayers){

            if(notifyply == ply)
                continue;

            notifyply.SendMessage(message);
        }
    }

    //! Broadcasts a message about player being challengeable to other players
    public void BroadcastChallengeable(Player ply, bool challengeable){

        auto message = new ClientAllowChallengeMessage(ply.Name, challengeable);
        
        foreach(notifyply; OnlinePlayers){

            if(notifyply == ply)
                continue;

            notifyply.SendMessage(message);
        }
    }

    //! Returns a random card
    public const const(Card) GetRandomCard(){

        return GameCards.GetRandom();
    }


    private void MatchStatusUpdater(){

        // Just run this forever //
        while(true){

            vibe.core.core.sleep(GAME_TICK_INTERVAL);

            for(int i = 0; i < OngoingGames.length; ){

                auto match = OngoingGames[i];
                
                if(match.UpdatePassedTime()){

                    // Match has completed //
                    OngoingGames = remove(OngoingGames, i);

                    PlayerExitMatchAllowChallenges(match.FirstPlayer);
                    PlayerExitMatchAllowChallenges(match.SecondPlayer);
                    
                } else {

                    ++i;
                }
            }
        }
    }

    //! Used to receive UNIX socket control signals
    private void ProcessUnixCommandSignals(){

        while(true){

            immutable cmd = receiveOnlyCompat!string();
            _ProcessCommand(cmd);
        }
    }

    //! Returns a response
    private void _ProcessCommand(in string cmd){

        logInfo("Processing local command: " ~ cmd);

        switch(cmd){
        case "versionupgrade":
        {
            BroadcastMessage("SERVER IS RESTARTING FOR UPGRADE!");
            SendMessageAll(new ServerGoingDownMessage(
                    "The server is updating to a new version." ~
                    " Please refresh the page in a few minutes."));
            return;
        }
        default:
            logError("Unknown command: " ~ cmd);
        }
    }

    //! Returns a player by name or null
    public @property Player GetPlayer(in string name){

        foreach(ply; OnlinePlayers){

            if(ply.Name == name)
                return ply;
        }

        return null;
    }

    //! Currently online players, all connections should properly call disconnect when
    //! players leave so this shouldn't contain players who have left
    private Player[] OnlinePlayers;

    //private Mutex MatchMutex;
    private Match[] OngoingGames;

    //! Runs a task forever that updates match timers
    private TaskWrapper MatchTask = null;

    //! Runs a task forever that receives messages from a local unix socket
    private TaskWrapper LocalControlTask = null;

    public CardManager GameCards;

    private ThreadManager GameThreads;
    
    public @property const(ThreadManager) Threads() const{

        return GameThreads;
    }
}

//alias GameManager = shared(GameManager_);
alias GameManager = GameManager_;
