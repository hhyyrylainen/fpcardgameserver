import messages;
import config;
import threads;

import std.json;
import std.file;
import std.random;
import std.exception;
import std.conv : to;

enum CardType {

    user,

    spell
}

// Type definition of a card
class Card {

    version(unittest){

        public this(){

            this(1);
        }

        public this(long id)
            in{
                assert(id > 0);
            }
        body{

            CardID = id;
            Name = "Testing card";
            ImagePath = "/images/cards/";
            Type = CardType.user;
        }

        public DrawnCard Draw() const{

            return new DrawnCard(this, 1, 0);
        }

        public DrawnCard Draw(long number) const{

            return new DrawnCard(this, number, 0);
        }
    }

    //! Creates a card from id number and definition
    public this(long id, in JSONValue data)
        in{
            assert(id > 0);
        }
    body{

        CardID = id;
        ImagePath = "/images/cards/" ~ data["image"].str;
        Description = data["description"].str;
        Name = data["name"].str;

        Type = CardTypeFromString(data["type"].str);

        if(Type == CardType.user){
            Attack = cast(int)data["attack"].integer;
            Defence = cast(int)data["defence"].integer;
        }

        if(Type == CardType.user || Type == CardType.spell){
            Cost = cast(int)data["cost"].integer;
        }

        if(Type == CardType.spell){

            Effect = PostEffect.Create(data["effect"].str);
        }
    }

    // Makes a drawn version of this card
    //! \param cardnumber The id for the drawn card
    public DrawnCard Draw(long cardnumber, int whose) const
        in{
            assert(cardnumber >= 0);
            assert(whose >= 0);
        }
    out(result){

        assert(result.InstanceAttack == this.Attack);
    }
    body{
        
        return new DrawnCard(this, cardnumber, whose);
    }

    // Serializes all public data about this card
    public JSONValue Serialize() const{

        JSONValue result;
        result["id"] = CardID;
        result["image"] = ImagePath;
        result["description"] = Description;
        result["name"] = Name;
        result["type"] = to!string(Type);

        if(Type == CardType.user){
            result["attack"] = Attack;
            result["defence"] = Defence;
        }

        if(Type == CardType.user || Type == CardType.spell){
            result["cost"] = Cost;
        }

        if(Type == CardType.spell){

            // TODO: check do we need to send the effect
        }

        return result;
    }

    public static CardType CardTypeFromString(in string str){

        return to!CardType(str);
    }

    unittest{

        assert(CardTypeFromString("user") == CardType.user);
        assert(CardTypeFromString("spell") == CardType.spell);
        assertThrown!Exception(CardTypeFromString("dda"));
    }

    // Card number. Must be over 0
    public const long CardID;

    //! Type of the card, defines which properties are valid
    public const CardType Type;

    //! Card's name
    public const string Name;

    // Relative path for client to load images from
    public const string ImagePath;

    // Description
    public const string Description = "Just a basic card";

    // Basic attack power of the card
    public const int Attack = 0;

    // Basic defence points of the card
    public const int Defence = 0;

    // Basic energy cost of the card
    public const int Cost = 0;

    //! If this is a spell this is valid
    PostEffect Effect = null;

    invariant{

        assert(CardID > 0);
    }

    unittest{

        auto card = new Card(1);

        auto serialized = parseJSON(card.Serialize.toString);

        assert(card.CardID == serialized["id"].integer);
        assert(card.Name == serialized["name"].str);
        assert(card.ImagePath == serialized["image"].str);
        assert(card.Description == serialized["description"].str);
        assert("user" == serialized["type"].str);
    }
}

// Card that is in play
class DrawnCard {

    private this(const Card fromcard, in long matchcard, in int whose)
        in{
            assert(fromcard !is null);
            assert(whose >= 0);
        }
    body{

        Template = fromcard;
        MatchCardNumber = matchcard;
        WhoseCard = whose;

        SetPropertiesToDefaults();
    }

    // Serializes this card to be sent. Connection holds a list of cards whose details have been
    // sent to allow not sending those again
    public JSONValue Serialize(SentCardCache cardcache, bool hidden) const{

        JSONValue result;
        
        if(!hidden){
            
            // Player's own card, tell them what card it is
            result["id"] = Template.CardID;
            result["hidden"] = false;

            if(cardcache !is null && cardcache.IsAlreadySent(Template)){

                // Just the id is fine //
                result["full"] = false;

            
            } else {

                // Need to send all the data //
                result["full"] = true;
                result["definition"] = Template.Serialize();            
            }
            
        } else {

            // Opponent's card, don't tell what it is //
            result["hidden"] = true;
        }
        
        result["matchcard"] = MatchCardNumber;

        return result;
    }

    public void SetPropertiesToDefaults(){

        InstanceAttack = Template.Attack;
        InstanceDefence = Template.Defence;
        InstanceCost = Template.Cost;
    }

    private const Card Template;

    // Instance properties taken from Template and possibly modified by gameplay actions
    public int InstanceAttack;

    // Basic defence points of the card
    public int InstanceDefence;

    //! Instance specific cost
    public int InstanceCost;

    // Set to true when a card has attacked once
    public bool AttackedThisTurn = false;

    //! Which player's card this is, 0 for First, 1 for Second
    public int WhoseCard = 0;

    // The card identifier in a match
    public const long MatchCardNumber = 0;

    //! True if the card has been killed ("banned")
    public bool Banned = false;

    //! True if the card is not in a players hand
    public bool Fielded = false;

    public @property long CardID() const{

        return Template.CardID;
    }

    public @property CardType Type() const{

        return Template.Type;
    }

    public @property const(PostEffect) Effect() const{

        return Template.Effect;
    }


    //! Returns true if this card has health left
    public const @property bool HasHealth(){

        return InstanceDefence > 0;
    }

    invariant{

        assert(Template !is null);
    }

    unittest{

        auto card = new Card(1);
        auto cache = new SentCardCache;

        auto drawn = card.Draw();

        auto serialized = drawn.Serialize(cache, false);

        assert(serialized["id"].integer == card.CardID);
        assert(serialized["matchcard"].integer == drawn.MatchCardNumber);
        assert(serialized["full"].type == JSON_TYPE.TRUE);
        assert(serialized["definition"]["id"].integer == card.CardID);
    }
}

//! Retrieves card objects based on names
class CardManager {

    //! Loads card definitions from configuration file
    public this(){

        string text = readText(CardConfig);
        auto cards = parseJSON(text);

        assert(cards.type == JSON_TYPE.ARRAY);

        foreach(JSONValue card; cards.array){
            
            GameCards ~= new Card(cast(int)card["id"].integer, card);
            
        }
    }

    //! Returns a random card
    public const const(Card) GetRandom(){

        return GameCards[uniform(0, GameCards.length)];
    }

    private Card[] GameCards;
}
