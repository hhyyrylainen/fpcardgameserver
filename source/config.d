import std.exception;

static immutable ServerVersion = "0.0.5";
static immutable CardConfig = "./Data/Cards.json";
static immutable ThreadConfig = "./Data/Threads.json";

static immutable DefaultChatAllowance = 5;

static immutable DefaultWinnerRatings = 10;

class MessageException : Exception{

    public this(const string message){

        super(message);
    }
}

static immutable TutorialMessage = "Quick Tutorial: Once your turn starts drag cards from your hand to the battlefield. Once you have played all your cards select one post from the list below. Then attack the opponent's cards by dragging with your cards. You can win by getting 100 winner ratings from posts. Good luck!";

