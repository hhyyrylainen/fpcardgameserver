import config;
import parsing;

import match;
import cards;

import std.json;
import std.file;
import std.random;
import std.exception;
import std.range;
import std.conv : to;
import std.string;

abstract class PostEffect {

    //! Creates a PostEffect from a string defining the effect
    public static PostEffect Create(in string serialized){

        const words = serialized.toLower.split;

        enforce(words.length > 0);

        switch(words[0]){
        case "winner":
        {
            enforce(words.length == 2);
            return new PEffectWinner(EffectParser.ParseMultiplier(words[1]));
        }
        case "disagree":
        {
            enforce(words.length == 2);
            return new PEffectDisagree(EffectParser.ParseMultiplier(words[1]));
        }
        case "unban":
        {
            enforce(words.length == 2);
            return new PEffectUnban(EffectParser.ParseMultiplier(words[1]));
        }
        default:
            throw new Exception("Unknown PostEffect type in serialized: " ~ words[0]);
        }
    }
    unittest{

        immutable winstr = "winner x3";
        const winning = Create(winstr);
        assert(winning.ToString == winstr);
        
    }

    //! Does the opposite of Create
    public abstract string ToString() const;

    //! Applies the effect
    //! throws if requires a target
    public abstract void Apply(Match match, Participant part) const;

    //! Applies the effect
    //! Does weird things if target hasn't been verified to be valid
    public abstract void Apply(Match match, Participant part, DrawnCard target) const;

    //! Checks if target is valid for this effect
    public abstract bool IsTargetValid(Match match, Participant part, DrawnCard target) const;


    public @property bool RequiresTarget() const { return false; }

    public @property bool CanTargetOwn() const { return true; }

    public @property bool CanTargetOpponent() const { return true; }
    
    // Help for AI //
    public @property bool AIShouldTargetOwn() const { return false; }

    public @property bool AIShouldTargetEnemy() const { return true; }

    // For clientside effects
    public @property string ClientEffectName() const { return "flame"; }

}

class Post {

    version(unittest){

        public this(uint chance){

            this("Get winning points", "Winner", "Best post", chance, 5,
                PostEffect.Create("winner x1"));
        }
    }

    public this(in string description, in string poster, in string text,
        uint chance, uint decay,
        in PostEffect effect, in string link = "")
    {
        Description = description;
        Poster = poster;
        Chance = chance;
        Decay = decay;
        Effect = effect;
        Link = link;
        Text = text;
    }

    public this(in JSONValue serialized){

        Link = serialized["link"].str;
        Description = serialized["description"].str;
        Poster = serialized["poster"].str;
        Text = serialized["text"].str;
        
        Chance = to!uint(serialized["chance"].integer);
        enforce(Chance > 0 && Chance <= 100);

        Decay = to!uint(serialized["decay"].integer);
        enforce(Decay > 0 && Decay <= 10);

        Effect = PostEffect.Create(serialized["effect"].str);
    }

    //! String displayed to player when selecting options
    public const string Description;
    public const string Poster;

    public const string Text;

    //! Chances of all posts are summed when selecting and then
    //! selecting based on each post's Chance meaning relative Chance
    //! to other posts will determine the most likely ones
    public const uint Chance;

    //! Number of player's turns until is no longer available
    public const uint Decay;
    public const PostEffect Effect;
    public const string Link;
}

//! A post that a player can play on their turn
class MatchPost {

    public this(const Post post, in long id){

        ID = id;
        PostData = post;
        TurnsLeft = PostData.Decay;
    }

    public const Post PostData;
    public const long ID;
    public int TurnsLeft;
}

class Thread {

    public this(in const(Post)[] posts, in string title, in string link = "")
        in{
            assert(posts.length > 0);
        }
    body{

        Posts = posts;
        Title = title;
        Link = link;

        uint weight = 0;
        foreach(post; Posts)
            weight += post.Chance;
        
        TotalWeights = weight;
    }
    
    public this(in JSONValue serialized){

        Title = serialized["title"].str;
        Link = serialized["link"].str;

        auto posts = serialized["posts"];

        assert(posts.type == JSON_TYPE.ARRAY);

        // Load posts //
        uint weight = 0;
        foreach(postJSON; posts.array){

            const post = new Post(postJSON);
            weight += post.Chance;
            Posts ~= post;
        }

        TotalWeights = weight;

        enforce(Posts.length > 0);
    }

    public @property ulong PostCount() const{

        return Posts.length;
    }

    public @property const(Post) GetPost(ulong i) const{

        enforce(i < Posts.length);
        return Posts[i];
    }

    public @property const(Post) RandomPost() const{

        int targetvalue = uniform(0, TotalWeights);

        long i;
        
        for(i = 0; i < Posts.length; ++i){

            if(targetvalue < Posts[i].Chance)
                break;
            
            targetvalue -= Posts[i].Chance;
        }

        return Posts[i];
    }

    unittest {

        auto posts = [new Post(10), new Post(30), new Post(40)];
        const thread = new Thread(posts, "test thread");

        foreach(i; 1..5){

            const thing = thread.RandomPost;
            assert(thing !is null);
        }

        assert(thread.Posts[thread.Posts.length - 1] is thread.Posts[$ - 1]);
    }

    public const string Title;
    public const string Link;

    public const uint TotalWeights;
    
    private const(Post)[] Posts;

    unittest {

        auto posts = [new Post(10)];
        const thread = new Thread(posts, "test thread");

        assert(thread.RandomPost !is null);

        assert(thread.Posts[thread.Posts.length - 1] is thread.Posts[$ - 1]);
    }
}

class ThreadManager {

    //! Loads all the Threads and Posts from the data json
    public this(){

        string text = readText(ThreadConfig);
        const threads = parseJSON(text);

        assert(threads.type == JSON_TYPE.ARRAY);

        foreach(threadJSON; threads.array){

            AllThreads ~= new Thread(threadJSON);
        }
    }

    public @property const(Thread) RandomThread() const{

        return AllThreads[uniform(0, AllThreads.length)];
    }

    private Thread[] AllThreads;

    unittest{

        auto manager = new ThreadManager;
        assert(manager.AllThreads.length > 0);

        assert(manager.AllThreads[0].Title.length > 0);
        assert(manager.AllThreads[0].Link.length > 0);
        assert(manager.AllThreads[0].PostCount > 0);
        assert(manager.AllThreads[0].GetPost(0).Link.length > 0);
        
        assert(manager.RandomThread !is null);
        assert(manager.RandomThread.RandomPost !is null);
    }

    unittest {

        import std.stdio;
        
        auto manager = new ThreadManager;

        auto thread = manager.RandomThread;

        foreach(i; 1..25){

            auto thing = thread.RandomPost;
            assert(thing !is null);
            //writeln("Thing: " ~ thing.Description);
        }
    }
}

class TargetLessEffect : PostEffect {
public:
    override void Apply(Match match, Participant part, DrawnCard target) const{

        throw new Exception("Effect doesn't use target");
    }

    override bool IsTargetValid(Match match, Participant part, DrawnCard target) const{
        throw new Exception("Tried to check target of a targetless effect");
    }
}

class TargetRequireEffect : PostEffect {
public:
    override void Apply(Match match, Participant part) const{

        throw new Exception("Effect requires a target");
    }

    override @property bool RequiresTarget() const { return true; }
}

////////////////////////////////////////////////////////////////////////////////

enum string ImplementEffectMultiplier = q{

    public this(int multiplier){

        Multiplier = multiplier;
    }

    protected int Multiplier;
};

class PEffectWinner : TargetLessEffect {
public:

    mixin(ImplementEffectMultiplier);
    
    override string ToString() const{

        return "winner x" ~ to!string(Multiplier);
    }

    override void Apply(Match match, Participant part) const{

        match.AddWinnerRatings(part, Multiplier);
    }
}


class PEffectDisagree : TargetRequireEffect {
public:
    mixin(ImplementEffectMultiplier);

    override string ToString() const{

        return "disagree x" ~ to!string(Multiplier);
    }

    override void Apply(Match match, Participant part, DrawnCard target) const{

        assert(IsTargetValid(match, part, target));

        match.CardDamaged(target, Multiplier, null);
    }

    override bool IsTargetValid(Match match, Participant part, DrawnCard target) const{

        if(target.Fielded && !target.Banned)
            return true;

        return false;
    }
}

class PEffectUnban : TargetRequireEffect {
public:

    mixin(ImplementEffectMultiplier);

    override string ToString() const{

        return "unban x" ~ to!string(Multiplier);
    }

    override void Apply(Match match, Participant part, DrawnCard target) const{

        assert(IsTargetValid(match, part, target));

        assert(Multiplier == 1, "TODO: multi use effects");
        
        match.Unban(target);
    }

    override bool IsTargetValid(Match match, Participant part, DrawnCard target) const{

        if(target.Banned && part.OwnsCard(target))
            return true;

        return false;
    }

    public @property override bool CanTargetOwn() const { return true; }

    public @property override bool CanTargetOpponent() const { return false; }

    public @property override string ClientEffectName() const { return "heal"; }
}
