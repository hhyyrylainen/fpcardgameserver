import core.time;

import std.string;
import std.exception;
import std.algorithm;
import std.uni;

import connection;
import messages;
import match;
import utility;
import config;

import ai;

// Reserved AI names
static immutable AIEasy = "[AI:1] Easy";
static immutable AIMedium = "[AI:2] Medium";
static immutable AIHard = "[AI:3] Hard";
static immutable AIBeatMe = "[AI:4] Punish Me Daddy";

enum AI_TYPE {

    EASY,
    MEDIUM,
    HARD,
    BEATME,
    // Not actually an AI
    PLAYER
}

abstract class BasePlayer {

    //! \returns False on error
    public abstract bool SendMessage(IMessage message);

    //! Sets PartInMatch on player to false if the player was in the match
    public abstract bool NoLongerInMatch(Match match);

    public abstract @property string Name();

    public abstract @property SentCardCache SentCards();
}

//! Represents a connected player
class Player : BasePlayer {

    public this(string name, SocketWrapper socket){

        if(name.length < 1)
            throw new Exception("Name is too short");

        // Cut name if too long //
        if(name.length > 50)
            name = name[0 .. 50];

        switch(name){
        case AIEasy:
        case AIMedium:
        case AIHard:
        case AIBeatMe:
            throw new Exception("Using reserved name");
        default: break;
        }

        PlayerName = name.removechars("[]").RemoveTags;

        if(PlayerName.length < 1){

            throw new Exception("Name contained only illegal characters");
        }

        if(PlayerName.toLower.startsWith("ai")){

            throw new Exception("Name may not start with AI");
        }

        Socket = socket;
        _SentCardCache = new SentCardCache;
        LastChatTime = MonoTime.currTime;
    }

    public static AI_TYPE GetAIFromString(in string name){

        switch(name){
        case AIEasy: return AI_TYPE.EASY;
        case AIMedium: return AI_TYPE.MEDIUM;
        case AIHard: return AI_TYPE.HARD;
        case AIBeatMe: return AI_TYPE.BEATME;
        default:
            return AI_TYPE.PLAYER;
        }
    }

    public override bool SendMessage(IMessage message){

        if(Socket is null)
            return false;
        
        return Socket.SendMessage(message);
    }

    public const string PlayerName;

    public override @property string Name(){

        return PlayerName;
    }

    public override @property SentCardCache SentCards(){

        return _SentCardCache;
    }

    public override bool NoLongerInMatch(Match match){

        if(PartInMatch is match){

            PartInMatch = null;
            return true;
        }

        return false;
    }

    private SocketWrapper Socket = null;

    private SentCardCache _SentCardCache = null;

    public Match PartInMatch = null;

    public @property bool IsInMatch() const{

        return PartInMatch !is null;
    }

    public string ChallengedPlayer = "";

    //! If false cannot receive challenge requests
    public bool AcceptingChallengeRequests = true;
    
    invariant{

        assert(PlayerName.length > 0);
    }

    // Name test
    unittest{

        Player ply = new Player("Cool dude 21", null);

        assert(ply.PlayerName == "Cool dude 21");

        assertThrown!Exception(new Player(AIEasy, null));

        assertThrown!Exception(new Player("[[]]][[[", null));

        assertThrown!Exception(new Player("[AI", null));

        assertThrown!Exception(new Player("AI", null));

        assertThrown!Exception(new Player("ai", null));

        ply = new Player("Cool dude [21][", null);

        assert(ply.PlayerName == "Cool dude 21");
    }

    // Long name test
    unittest{

        immutable name = "A guy with a very long name that will be cut to make it shorter " ~
            "and under 50 characters which will be less than this";
        Player ply = new Player(name, null);

        assert(ply.Name.length < name.length);
    }

    // Rate limiting for chat
    public @property bool RateLimitChat(){

        // Add one to allowance per 1 seconds or reset entirely after 5 seconds //
        auto sinceLastMessage = MonoTime.currTime - LastChatTime;

        if(sinceLastMessage >= dur!"seconds"(5)){

            ChatAllowance = DefaultChatAllowance;
        } else if(sinceLastMessage >= dur!"seconds"(1)){

            ChatAllowance = max(ChatAllowance + 1, DefaultChatAllowance);
        }

        // Update last chat time //
        // This forces spammers to stop entirely for a few seconds //
        LastChatTime = MonoTime.currTime;
        
        --ChatAllowance;

        // Actual limit check //
        if(ChatAllowance < 0){

            ChatAllowance = 0;
            return true;
        }

        return false;
    }

    // Last chat message sent timestamp
    private MonoTime LastChatTime;
    private int ChatAllowance = DefaultChatAllowance;

    // Rate limit kicks in after DefaultChatAllowance messages
    unittest{

        immutable name = "test name";
        Player ply = new Player(name, null);

        foreach(i; 0..DefaultChatAllowance){

            assert(ply.RateLimitChat == false);
        }

        // Now it should rate limit //
        assert(ply.RateLimitChat == true);

        // Reset time //
        ply.LastChatTime = MonoTime.currTime - dur!"seconds"(2);
        assert(ply.RateLimitChat == false);
    }
    

    unittest{

        import cards;
        
        auto card1 = new Card();
        auto card2 = new Card();

        Player ply1 = new Player("Guy 1", null);

        assert(!ply1.SentCards.IsAlreadySent(card1));
        assert(ply1.SentCards.IsAlreadySent(card1));

        Player ply2 = new Player("Guy 1", null);

        assert(!ply2.SentCards.IsAlreadySent(card1));
        assert(ply2.SentCards.IsAlreadySent(card1));

        assert(!ply2.SentCards.IsAlreadySent(card2));
        assert(ply2.SentCards.IsAlreadySent(card2));

        assert(ply1.SentCards.IsAlreadySent(card1));
        assert(!ply1.SentCards.IsAlreadySent(card2));
        assert(ply1.SentCards.IsAlreadySent(card2));
    }
}

//! Represents an AI instance
class AI : BasePlayer {

    public this(in AI_TYPE type){

        enforce(type != AI_TYPE.PLAYER);

        Type = type;
    }

    public override bool SendMessage(IMessage message) {

        // AI's don't need messages
        return true;
    }

    public override bool NoLongerInMatch(Match match){

        return false;
    }

    public override @property string Name(){

        switch(Type){
        case AI_TYPE.EASY:
            return AIEasy;
        case AI_TYPE.MEDIUM:
            return AIMedium;
        case AI_TYPE.HARD:
            return AIHard;
        case AI_TYPE.BEATME:
            return AIBeatMe;
        default:
            return "AI";
        }
    }

    public override @property SentCardCache SentCards(){

        return null;
    }

    public AI_TYPE Type;

    public AIData Data = null;

    unittest{

        assertThrown(new AI(AI_TYPE.PLAYER));

        auto ai = new AI(AI_TYPE.MEDIUM);

        assert(ai.Name == AIMedium);
    }
}


version(unittest){

    //! Unittesting helper for message send
    class SendMessageCollector : BasePlayer{

        public override bool SendMessage(IMessage message) {

            int* existing = message.Type in ReceivedThings;

            if(existing){

                *existing += 1;

            } else {

                // New value //
                ReceivedThings[message.Type] = 1;
            }
            
            return true;
        }

        public override bool NoLongerInMatch(Match match){

            return false;
        }

        public void Reset(){

            ReceivedThings = (int[MessageType]).init;
        }

        public alias Clear = Reset;

        public override @property string Name(){

            return "SendMessageCollector";
        }

        public override @property SentCardCache SentCards(){

            return null;
        }

        public int[MessageType] ReceivedThings;

        public @property int MessageCount(){

            int total = 0;

            foreach(typeCount; ReceivedThings.values){

                total += typeCount;
            }

            return total;
        }

        public @property bool Empty(){

            return MessageCount == 0;
        }
    }
}

