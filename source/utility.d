//! \file Common utility functions
import std.array;
import std.string;
import std.algorithm;


//! Replaces < and > with html entities
public string RemoveTags(in string str){

    return str.replace("<", "&lt;").replace(">", "gt;t").replace("&", "&amp;").
        replace("\"", "&quot;").replace("'", "&#x27;").replace("/", "&#x2F;");
}

unittest{

    string teststr = "<b>Nice tags</b>";

    assert(RemoveTags(teststr).indexOf('<') == -1);
}
