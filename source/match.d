// Represents a single match between 1-2 players
import connection;
import player;
import games;
import cards;
import threads;
import messages;

import config;

import ai;

import std.exception;
import core.time;
import std.algorithm;
import std.conv;
import std.stdio;
import std.array;

import vibe.core.log;


// Keeps track for match state for turns and dealing cards etc
enum MATCH_STATE {
    
    // Waiting to deal initial cards //
    PRE,

    // Dealing cards, can start once client animations finish. In about 5 seconds
    DEALING_INITIAL,

    // First player's turn
    TURN_PLY1,

    // Second player's turn
    TURN_PLY2,

    // At this point the states stop progressing and go back to TURN_PLY1


    // Somebody has won, wait for final animations about 7 seconds
    POST,

    // Match has ended, clients can now go back to MAIN and delete this object
    ENDED
}



class Match {

    private this(const GameManager manager){

        GamesHost = manager;

        // Select thread //
        MatchThread = GamesHost.Threads.RandomThread;

        ResetTime;
    }

    public this(Player ply, AI ai, const GameManager manager){

        this(manager);

        First = new Participant(ply, 0);
        Second = new Participant(ai, 1);

        assert(First.PlayerNumber == 0);
        assert(Second.PlayerNumber == 1);

        enforce(ply.PartInMatch is null);
        ply.PartInMatch = this;
    }

    // PVP match
    public this(Player ply1, Player ply2, const GameManager manager){

        this(manager);

        First = new Participant(ply1, 0);
        Second = new Participant(ply2, 1);

        enforce(ply1.PartInMatch is null);
        enforce(ply2.PartInMatch is null);
        ply1.PartInMatch = this;
        ply2.PartInMatch = this;
    }

    //! AI-only match, used for testing
    public this(AI ai1, AI ai2, const GameManager manager){

        this(manager);

        First = new Participant(ai1, 0);
        Second = new Participant(ai2, 1);

        ResetTime;
    }

    //! A player that might be in this match is no longer valid
    public void PossiblyRelevantPlayerLeft(Player ply){

        if(ply == First._Player){

            First._Player = null;
            ArePlayersValid = false;
            writeln("Player1 left from match");
            
        } else if(ply == Second._Player){

            Second._Player = null;
            ArePlayersValid = false;
            writeln("Player2 left from match");
        }
    }

    //! A player who definitely was in this match left
    public void PlayerLeft(Player ply){

        enforce(ply.PartInMatch == this);

        if(ply == First._Player){

            ply.PartInMatch = null;
            First._Player = null;
            ArePlayersValid = false;
            writeln("Player1 left from match");
            return;
            
        } else if(ply == Second._Player){

            ply.PartInMatch = null;
            Second._Player = null;
            ArePlayersValid = false;
            writeln("Player2 left from match");
            return;
        }

        throw new Exception("ply not part of match");
    }

    //! \returns True if match can be deleted (is finished)
    public bool UpdatePassedTime(){

        // Send queued messages //
        // Time must be always GAME_TICK_INTERVAL
        First.HandleQueuedMessages(GAME_TICK_INTERVAL);
        Second.HandleQueuedMessages(GAME_TICK_INTERVAL);

        // Match end always works //
        if(State == MATCH_STATE.ENDED){

            // Send end messages //
            writeln("a match has ended");

            if(First._Player !is null)
                First._Player.NoLongerInMatch(this);

            if(Second._Player !is null)
                Second._Player.NoLongerInMatch(this);
            
            SendMessage(new ClientExitMatchMessage());

            // Flush messages
            First.FlushQueue();
            Second.FlushQueue();
            
            return true;
        }

        // Win by rage quit //
        if(!ArePlayersValid){

            if(State != MATCH_STATE.POST){

                // Move to post match immediately //
                State = MATCH_STATE.POST;
                
                // Send message to still active players //
                if(First.IsConnected){

                    // First won //
                    First.SendMessage(new MatchEndMessage("Opponent Disconnected", true, 0));
                    
                } else if(Second.IsConnected){

                    // Second won //
                    Second.SendMessage(new MatchEndMessage("Opponent Disconnected", true, 0));
                }
            }

            if(TimeSinceReset >= seconds(7)){

                State = MATCH_STATE.ENDED;
            }

            return false;
        }        

        switch(State){
        case MATCH_STATE.PRE:
        {
            if(!ThreadSent){

                ThreadSent = true;
                
                SendMessage(new ThreadSelectMessage(MatchThread));

                // Send a tutorial / demo post
                SendMessage(new NewPostDoneMessage(new MatchPost(
                            new Post("TutorialMessage", "Boost", TutorialMessage,
                                0, 0, new PEffectWinner(100)),
                            -1)));

                // Send a rules message //
                SendMessage(new GameRuleSettingsMessage(WinnerRatingsToVictory));
            }
            
            if(TimeSinceReset >= msecs(1300)){

                assert(ThreadSent);
                writeln("Dealing cards in match");

                // Deal initial cards //
                DrawCards(First);
                DrawCards(Second);
                
                State = MATCH_STATE.DEALING_INITIAL;
                
            } 
        }
        break;
        case MATCH_STATE.DEALING_INITIAL:
        {
            if(TimeSinceReset >= seconds(3)){

                State = MATCH_STATE.TURN_PLY1;
            }
        }
        break;
        case MATCH_STATE.POST:
        {
            if(TimeSinceReset >= seconds(7)){

                State = MATCH_STATE.ENDED;
            }
        }
        break;
        case MATCH_STATE.TURN_PLY2:
        case MATCH_STATE.TURN_PLY1:
        {
            if(TimeSinceReset >= seconds(65)){

                // Timed out //
                ChangeTurn();
                
            } else {

                // AI //
                if(TimeSinceReset >= seconds(4)){
                    
                    auto part = CurrentPlayer;

                    if(part !is null && part.IsAI){

                        // AI Do something //
                        if(AIDecisionMaker.PlayTurn(cast(AI)(part._Player),
                                                    this, part, TimeSinceReset))
                        {
                            // AI finished //
                            ChangeTurn();
                            
                        } else {

                            // AI is waiting for player to see animations //
                            
                        }

                    }
                }
            }
        }
        break;
        default: throw new Exception("Invalid match state");
        }

        return false;
    }

    // Makes the turn swap to the other player
    public void ChangeTurn(){

        if(State == MATCH_STATE.TURN_PLY1)
            State = MATCH_STATE.TURN_PLY2;
        else
            State = MATCH_STATE.TURN_PLY1;
    }

    public auto TimeSinceReset() @property{

        return MonoTime.currTime - Timer;
    }

    public void ResetTime(){

        Timer = MonoTime.currTime;
    }
    
    public Participant GetParticipant(Player ply){

        if(ply == First._Player)
            return First;

        if(ply == Second._Player)
            return Second;
        
        throw new Exception("ply not part of match");
    }

    //! Returns the participant whose turn it currently is, or null
    public @property Participant CurrentPlayer(){

        if(State == MATCH_STATE.TURN_PLY1)
            return First;
        if(State == MATCH_STATE.TURN_PLY2)
            return Second;
        
        return null;
    }

    //! Gets the participant that isn't other
    public Participant GetOther(Participant other){

        if(other == First)
            return Second;

        return First;
    }

    //! Gets the participant who own's a card
    public Participant GetCardOwner(DrawnCard card){


        if(First.OwnsCard(card))
            return First;

        if(Second.OwnsCard(card))
            return Second;

        return null;
    }

    //! Sends a message to all players
    public void SendMessage(IMessage message){

        First.SendMessage(message);
        Second.SendMessage(message);
    }

    public DrawnCard GetFieldedCard(long matchid){

        auto card = First.GetCardOnField(matchid);

        if(card)
            return card;

        return Second.GetCardOnField(matchid);
    }

    public DrawnCard[] GetAllCards(){

        return First.AllCards ~ Second.AllCards;
    }

    //! A player ends turn
    public void EndTurnFor(Player ply){

        if(First._Player == ply && State == MATCH_STATE.TURN_PLY1){

            State = MATCH_STATE.TURN_PLY2;

        } else if(Second._Player == ply && State == MATCH_STATE.TURN_PLY2){

            State = MATCH_STATE.TURN_PLY1;
        }
    }

    //! Play card action
    public void DoAction(Player ply, PlayCardAction action){

        auto part = CurrentPlayer;

        if(part._Player != ply){

            // Not own turn //
            part.SetActionStatus(action.SequenceNumber, false);
            return;
        }

        // Find the card //
        auto card = part.GetCardInHand(action.MatchCardNumber);

        if(card is null || !CanPlayCard(part, card)){

            part.SetActionStatus(action.SequenceNumber, false);
            return;
        }

        PlayCard(card, part, action);
        
        writeln(ply.Name ~ " played a card");
    }

    //! Card attack
    public void DoAction(Player ply, AttackAction action){

        auto part = CurrentPlayer;

        if(part._Player != ply){

            // Not own turn //
            part.SetActionStatus(action.SequenceNumber, false);
            return;
        }

        // Find the card //
        auto card = part.GetCardOnField(action.Attacker);

        if(card is null){

            // Probably trying to cheat at this point...
            part.SetActionStatus(action.SequenceNumber, false);
            return;
        }

        // Find target card //
        auto target = GetFieldedCard(action.Target);

        if(target is null){

            // Attacked card has disappeared
            part.SetActionStatus(action.SequenceNumber, false);
            return;
        }

        AttackCard(part, card, target, action);
    }

    //! Doing post
    public void DoAction(Player ply, PostAction action){

        // Check that it is ply's turn //
        auto part = CurrentPlayer;

        if(part._Player != ply || part.PostedMessage){
            
            // Not own turn or has already posted //
            part.SetActionStatus(action.SequenceNumber, false);
            return;
        }

        // Find the post //
        MatchPost post = part.Post(action.MatchPostID);

        if(post is null){

            // Player tried to do an invalid post //
            part.SetActionStatus(action.SequenceNumber, false);
            return;
        }

        assert(post.ID == action.MatchPostID);
        
        part.SetActionStatus(action.SequenceNumber, true);

        // Apply the effect //
        if(!ApplyPostEffect(part, post.PostData.Effect)){

            // Player is now authorized to do a targeted post effect //
            part.RequireTarget(post.PostData.Effect);
        }
        
        // It is considered to be used even if there are no valid targets //
        part.MarkPostAsPosted(post);
        
        SendMessage(new NewPostDoneMessage(post));

        assert(part.PossiblePosts.length < 3);
    }

    //! Selected target for something
    public void DoAction(Player ply, TargetSelectedAction action){

        // Check that it is ply's turn //
        auto part = CurrentPlayer;

        if(part._Player != ply){

            // Not own turn //
            part.SetActionStatus(action.SequenceNumber, false);
            return;
        }

        // Silently fail, if ply doesn't have a pending action //
        if(!part.HasPendingTarget){

            part.SetActionStatus(action.SequenceNumber, true);
            return;
        }

        // Find target //
        switch(action.TargetType){
        case "card":
        {
            DrawnCard target = GetMatchCard(action.TargetID);

            if(target is null){

                // Target not valid //
                writeln("Didn't find target: " ~ to!string(action.TargetID));
                part.SetActionStatus(action.SequenceNumber, false);
                return;
            }

            // Target found, check can the effect apply //
            if(!ApplyPostEffect(part, part.PendingTarget.Effect, target)){

                // Invalid target! //
                part.SetActionStatus(action.SequenceNumber, false);
                return;
            }

            // Succeeded //
            part.PendingTarget = null;
            part.SetActionStatus(action.SequenceNumber, true);
            return;
        }
        default:
            // Unknown target type
            writeln("Unknown target type: " ~ to!string(action.TargetType));
            part.SetActionStatus(action.SequenceNumber, false);
            return;
        }

        assert(false);
    }

    public void AIApplyTargetSelect(Participant part, DrawnCard target){

        if(!ApplyPostEffect(part, part.PendingTarget.Effect, target)){

            writeln("[ERROR] Ai failed to apply effect to target");
        }

        part.PendingTarget = null;
    }

    //! A player don't want to use an effect that they are entitled to
    public void PlayerTryDiscardEffect(Player ply){

        auto part = GetParticipant(ply);
        
        // Silently fail, if ply doesn't have a pending action //
        if(part.PendingTarget is null){

            return;
        }

        // Allow canceling on not own turn //
        if(part._Player == CurrentPlayer && !CanCancel(part, part.PendingTarget.Effect)){

            part.RequireTarget(part.PendingTarget.Effect);
            return;
        }
        
        part.PendingTarget = null;
        
        // It is now canceled //
    }

    public bool CanCancel(Participant part, const(PostEffect) effect){

        return true;
    }
    
    // A player has given up, make the other win
    public void GiveUp(Player ply){

        if(ply == First._Player){

            // Second won //
            State = MATCH_STATE.POST;
                
            First.SendMessage(new MatchEndMessage("You Gave Up", false, 0));
            Second.SendMessage(new MatchEndMessage("Opponent Gave Up", true, 0));
            
            return;
        }

        if(ply == Second._Player){

            // First won //
            State = MATCH_STATE.POST;
                
            Second.SendMessage(new MatchEndMessage("You Gave Up", false, 0));
            First.SendMessage(new MatchEndMessage("Opponent Gave Up", true, 0));

            return;
        }

        throw new Exception("ply not part of match");
    }

    public void AddWinnerRatings(Participant part, int count){

        part.WinnerRatings += count;

        // Send an update message //
        auto other = GetOther(part);

        part.SendMessage(new WinnerRatingsUpdatedMessage(true, part.WinnerRatings));
        other.SendMessage(new WinnerRatingsUpdatedMessage(false, part.WinnerRatings));

        VictoryCheck();
    }

    //! Checks if any vicory condition is fullfilled
    public void VictoryCheck(){

        if(First.WinnerRatings >= WinnerRatingsToVictory){

            // First won //
            WinByRatings(First);
            
        } else if(Second.WinnerRatings >= WinnerRatingsToVictory){

            // Second won //
            WinByRatings(Second);
        }
    }

    public void WinByRatings(Participant part){

        State = MATCH_STATE.POST;

        // Winner //
        part.SendMessage(new MatchEndMessage("You Are the Winner x" ~
                to!string(WinnerRatingsToVictory), true, Second.WinnerRatings));

        // Opponent loses //
        GetOther(part).SendMessage(
            new MatchEndMessage("Opponent Reached Winner x" ~
                to!string(WinnerRatingsToVictory), false, First.WinnerRatings));

    }

    // Gameplay logic //

    //! Returns all cards a card can attack
    public DrawnCard[] FindTargetsCardCanAttack(Participant cardowner, DrawnCard attacker){

        return array(GetAllCards().filter!(a => CanAttack(cardowner, attacker, a)));
    }

    //! Returns all card targets a post can effect
    public DrawnCard[] FindTargetsForPost(Participant part, const PostEffect post){

        return array(GetAllCards().filter!(a => post.IsTargetValid(this, part, a)));
    }

    public DrawnCard[] FindTargetsForSpell(Participant part, const PostEffect spell){

        return FindTargetsForPost(part, spell);
    }

    public bool FindTargetIsPreferred(Participant part, DrawnCard target,
        const PostEffect effect) const
    {

        if(effect.AIShouldTargetOwn && part.OwnsCard(target))
            return true;

        if(effect.AIShouldTargetEnemy && !part.OwnsCard(target))
            return true;

        return false;
    }
    
    public void DrawCards(Participant player, int count = 5){

        ulong startindex = player.CountCardsInHand;

        for(int i = 0; i < count; ++i){

            player.AddCard(GamesHost.GetRandomCard().Draw(++DrawnID, player.PlayerNumber));
        }

        auto other = GetOther(player);
        
        player.SendMessage(player.CreateDrawMessage(startindex, true, player));
        other.SendMessage(player.CreateDrawMessage(startindex, false, other));

    }

    //! Fills possible posts for Participant
    public void FillPosts(Participant part){

        assert(MatchThread !is null);

        // Decay posts //
        foreach(post; part.PossiblePosts){

            if(--post.TurnsLeft < 1){
                part.SendMessage(new PostDecayedMessage(post));
            }
        }

        part._PossiblePosts = part.PossiblePosts.remove!(a => a.TurnsLeft < 1,
            SwapStrategy.unstable)();

        
        // Add new ones until full //
        while(part.PossiblePosts.length < 3){

            auto newpost = new MatchPost(MatchThread.RandomPost, ++PostID);

            part.SendMessage(new PostDrawnMessage(newpost));
            
            part._PossiblePosts ~= newpost;
        }
    }

    public void TurnStartEnergy(Participant player){

        // Calculate new energy //
        SetParticipantEnergy(player, player.Energy + 2 + cast(int)player.CountNonBannedCards);
    }

    public void SetParticipantEnergy(Participant player, int newenergy){

        player.Energy = newenergy;

        // Update players //
        player.SendMessage(new EnergyUpdatedMessage(true, player.Energy));
        GetOther(player).SendMessage(new EnergyUpdatedMessage(false, player.Energy));        
    }

    //! Returns a card if the matchid is valid
    public DrawnCard GetMatchCard(long matchid){

        auto card = First.GetOwnedCard(matchid);

        if(card !is null)
            return card;
        
        card = First.GetOwnedCard(matchid);

        if(card !is null)
            return card;
        
        card = Second.GetOwnedCard(matchid);

        if(card !is null)
            return card;
        
        card = Second.GetOwnedCard(matchid);

        return card;
    }

    //! Returns true if participant can play a card
    public bool CanPlayCard(Participant part, DrawnCard card){

        if(part is null || card is null)
            return false;

        // Can't play cards if need to select a target
        if(part.PendingTarget !is null)
            return false;

        // Energy cost
        if(part.Energy < card.InstanceCost)
            return false;
        
        // TODO: checks
        
        return true;
    }

    public bool CanAttack(Participant attackingCardOwner, DrawnCard attacker, DrawnCard target){

        // Fail if pending targeting spell //
        if(attackingCardOwner.PendingTarget !is null)
            return false;

        if(!attacker.Fielded || !target.Fielded)
            return false;
        
        if(attacker.AttackedThisTurn || !attacker.HasHealth || attacker.Banned)
            return false;

        if(attacker.WhoseCard == target.WhoseCard){
            // Attacking own card //
            return false;
        }

        if(!target.HasHealth || target.Banned)
            return false;
        
        return true;
    }

    public void PlayCard(DrawnCard card, Participant ply, PlayCardAction action = null){

        auto other = GetOther(ply);
        
        switch(card.Type){
        case CardType.user:
        {
            ply.FieldCard(card); 

            // Send a play action to the opponent //
            other.SendMessage(new PlayCardAction(card.MatchCardNumber, card.CardID, -1));

            // TODO: apply effects
        }
        break;
        case CardType.spell:
        {
            // Apply the effect //
            if(!ApplyPostEffect(ply, card.Effect)){

                // Player now needs to send a target message //
                ply.RequireTarget(card.Effect);
            }

            MoveToVoid(card);
        }
        break;
        default:
            if(action !is null)
                ply.SetActionStatus(action.SequenceNumber, false);
            throw new Exception("PlayCard for type unimplemented: " ~ to!string(card.Type));
        }
        
        // Report success before effects to make sure that clients place the card on
        // the field before getting effects
        if(action !is null)
            ply.SetActionStatus(action.SequenceNumber, true);
        
        // Also reveal the card to their opponent //
        other.SendMessage(new CardRevealMessage(card, other._Player.SentCards));

        // Energy cost
        SetParticipantEnergy(ply, ply.Energy - card.InstanceCost);
    }
    
    public void AttackCard(Participant cardowner, DrawnCard attacker, DrawnCard target,
                           AttackAction action = null)
        {
            // Check can attack //
            if(!CanAttack(cardowner, attacker, target)){

                if(action !is null)
                    cardowner.SetActionStatus(action.SequenceNumber, false);
                return;
            }

            attacker.AttackedThisTurn = true;
            
            if(action !is null)
                cardowner.SetActionStatus(action.SequenceNumber, true);

            // Send an action to the opponent //
            auto other = GetOther(cardowner);
            other.SendMessage(new AttackAction(attacker.MatchCardNumber,
                    target.MatchCardNumber, -1));
            
            // Apply damage //
            cardowner.SendDelay(GAME_TICK_INTERVAL / 2);
            other.SendDelay(GAME_TICK_INTERVAL / 2);
            CardDamaged(target, attacker.InstanceAttack, attacker);
        }

    //! Applies damage to a card, potentially killing it
    public void CardDamaged(DrawnCard target, int attackvalue, DrawnCard attacker = null){

        // Send damage message //
        target.InstanceDefence -= attackvalue;
        SendMessage(new CardStatsUpdatedMessage(target));
        
        

        // Check did the card get banned //
        if(!target.HasHealth){

            CardDied(target);
            
        }

        // Attacker might also take damage
        if(attacker !is null){

            //SendMessage(new CardStatsUpdatedMessage(attacker));

            if(!attacker.HasHealth){

                CardDied(attacker);

            }
        }
    }

    //! Removes a card completely from the game
    public void MoveToVoid(DrawnCard card){

        First.VoidCard(card);
        Second.VoidCard(card);
        
        SendMessage(new CardVoidedMessage(card.MatchCardNumber));
    }

    //! Removes a card from the field to the banned camp
    public void CardDied(DrawnCard card){

        CardSetEffects(card, false);

        card.Banned = true;

        SendMessage(new CardBannedStatusMessage(card.MatchCardNumber, 1, false));
    }

    //! Unbans a card, returns to original owner's field
    public void Unban(DrawnCard card){

        CardSetEffects(card, false);

        card.AttackedThisTurn = true;

        card.Banned = false;

        // Restore health //
        card.SetPropertiesToDefaults();
        SendMessage(new CardStatsUpdatedMessage(card));

        auto owner = GetCardOwner(card);

        enforce(owner !is null);
        
        owner.SendMessage(new CardBannedStatusMessage(card.MatchCardNumber, 0, true));
        
        GetOther(owner).SendMessage(
            new CardBannedStatusMessage(card.MatchCardNumber, 0, false));
    }

    //! Disables or enables a card effect
    public void CardSetEffects(DrawnCard card, bool enabled){

    }

    //! AI does a post
    public bool AIDoPost(Participant part, const MatchPost post,
                         const(PostEffect) effect, DrawnCard target = null)
        {
            assert(!part.PostedMessage);
            
            if(ApplyPostEffect(part, effect, target)){

                part.MarkPostAsPosted(post);
                SendMessage(new NewPostDoneMessage(post));
                return true;
            }

            return false;
        }
    
    //! Applies a post effect, activated by a player
    //! returns true if applied, false if player needs to choose a target (or target is invalid)
    public bool ApplyPostEffect(Participant part, const(PostEffect) effect,
        DrawnCard target = null)
    {

        // Can run it now if doesn't need a target //
        if(!effect.RequiresTarget){

            effect.Apply(this, part);
            return true;
        }
        
        // Check that the target is valid for the effect //
        if(target is null || !effect.IsTargetValid(this, part, target))
            return false;

        // Can apply the effect //
        effect.Apply(this, part, target);

        // Send effect message //
        SendMessage(new PlaySpellEffectMessage(effect.ClientEffectName,
                target.MatchCardNumber, "forum"));
        
        return true;
    }

    public @property Player FirstPlayer() const{

        return cast(Player)First._Player;
    }

    public @property Player SecondPlayer() const{

        return cast(Player)Second._Player;
    }

    private const GameManager GamesHost;

    private int DrawnID = 0;
    private long PostID = 0;

    //! True as long as no player has disconnected
    private bool ArePlayersValid = true;
    private Participant First;
    private Participant Second;

    private const AI_TYPE UsingAI = AI_TYPE.PLAYER;

    private MATCH_STATE _State = MATCH_STATE.PRE;

    private int _TurnNumber = 0;

    private const Thread MatchThread;

    private bool ThreadSent = false;

    // Win conditions //
    private const int WinnerRatingsToVictory = DefaultWinnerRatings;

    public @property int TurnNumber(){

        return _TurnNumber;
    }

    public @property void State(MATCH_STATE newstate){

        _State = newstate;
        ResetTime;

        if(_State == MATCH_STATE.TURN_PLY1){

            // Reset timers and notify players //
            Second.TurnEnded();

            // TurnChangeMessages forcefully go through delays
            First.SendMessageInternal(new TurnChangeMessage(_State, true, "Player 1's turn"));
            Second.SendMessageInternal(new TurnChangeMessage(_State, false,
                    "Player 1's turn"));

            DrawCards(First, 1);
            First.ResetAttacked();
            First.PostedMessage = false;
            TurnStartEnergy(First);
            FillPosts(First);

            ++_TurnNumber;
            
        } else if(_State == MATCH_STATE.TURN_PLY2){

            // Reset timers and notify players //
            First.TurnEnded();

            // TurnChangeMessages forcefully go through delays
            First.SendMessageInternal(new TurnChangeMessage(_State, true, "Player 2's turn"));
            Second.SendMessageInternal(new TurnChangeMessage(_State, false,
                    "Player 2's turn"));

            DrawCards(Second, 1);
            Second.ResetAttacked();
            Second.PostedMessage = false;
            TurnStartEnergy(Second);
            FillPosts(Second);

            ++_TurnNumber;
            
        } else {

            // Cancel both player's pending actions
            First.TurnEnded();
            Second.TurnEnded();
        }
    }

    public @property MATCH_STATE State(){

        return _State;
    }

    private MonoTime Timer;

    invariant {

        assert(MatchThread !is null);
        assert(GamesHost !is null);
        
        assert(_TurnNumber >= 0);
        assert(DrawnID >= 0);
    }
}



//! AI or player in a match
class Participant {

    class WaitingTarget {

        this(const PostEffect effect){

            Effect = effect;
        }
        
        const PostEffect Effect;
    }

    public this(Player ply, int plynumber){

        _Player = ply;
        PlayerNumber = plynumber;
    }

    public this(AI ai, int plynumber){

        _Player = ai;
        PlayerNumber = plynumber;
        IsAI = true;
    }

    version(unittest){
        
        // Testing SendMessage
        public this(BasePlayer ply){

            _Player = ply;
            PlayerNumber = 1;
            IsAI = false;
        }
    }


    //
    // Message sending
    //
    
    public DrawHandMessage CreateDrawMessage(ulong startindex, bool own,
        Participant forplayer)
    {
        if(startindex < 1)
            return new DrawHandMessage(to!int(CardsInHand.length), !own,
                cast(immutable)CardsInHand,
                forplayer._Player.SentCards);
        
        return new DrawHandMessage(to!int(CardsInHand.length - startindex), !own,
            cast(immutable)CardsInHand[startindex .. $],
            forplayer._Player.SentCards);
    }

    //! \returns True if no error occured. An error is a player whose connection is closed
    private bool SendMessageInternal(IMessage message){

        if(_Player is null)
            return false;

        return _Player.SendMessage(message);
    }

    //! Sends a status message for an action
    //! This is sent immediately for fast feedback to player
    public void SetActionStatus(long actionnumber, bool succeeded){

        SendMessageInternal(new ActionFinishedMessage(actionnumber, succeeded));
    }

    //! Sends a message
    //! Is sent immediately if queue is empty
    public void SendMessage(IMessage message){

        // AI ignored delays
        if(MessageQueue.empty || IsAI){

            SendMessageInternal(message);
            
        } else {

            MessageQueue ~= new MessageQueueItem(message);
        }
    }

    //! Sends a message with a delay
    public void SendMessage(IMessage message, Duration delay){

        MessageQueue ~= new MessageQueueItem(message, delay);
    }

    //! Adds a delay before next message can be sent
    public void SendDelay(Duration delay){

        MessageQueue ~= new MessageQueueItem(delay);
    }

    //! Sends all queued messages for which the time has elapsed
    private void HandleQueuedMessages(Duration delta){

        // Ai doesn't use delays
        if(IsAI || MessageQueue.empty)
            return;

        DelayAccumulator += delta;

        while(DelayAccumulator > 0.msecs && !MessageQueue.empty){

            MessageQueueItem[] doneMessages;

            for(int i = 0; i < MessageQueue.length; ++i){

                auto msg = MessageQueue[i];

                if(msg.Delay == 0.msecs || msg.Delay <= DelayAccumulator){

                    DelayAccumulator -= msg.Delay;

                    // Send
                    if(msg.Message !is null)
                        SendMessageInternal(msg.Message);
                    doneMessages ~= msg; 

                    // Stop if ran out of time
                    if(DelayAccumulator <= 0.msecs)
                        break;
                }
            }

            // Quit if didn't send anything
            if(doneMessages.empty())
                break;

            auto newmessages = MessageQueue.remove!(a => doneMessages.canFind(a),
                SwapStrategy.stable)();

            assert(newmessages.length != MessageQueue.length);
            MessageQueue = newmessages;
        }

        // Reset delay if all messages are sent //
        if(MessageQueue.empty)
            DelayAccumulator = 0.msecs;
    }

    //! Sends all queued messages ignoring delays
    private void FlushQueue(){

        if(MessageQueue.empty)
            return;

        logDebug("Flushing MessageQueue");

        foreach(msg; MessageQueue){

            SendMessageInternal(msg.Message);
        }
        
        MessageQueue = new MessageQueueItem[0];
    }

    
    //
    // Gameplay and card actions
    //

    //! \returns A card matching match id, or null if not in this player's hand
    public DrawnCard GetCardInHand(long matchid){

        foreach(card; CardsInHand){

            if(card.MatchCardNumber == matchid){

                return card;
            }
        }

        return null;
    }

    //! \returns A card matching match id, or null if not on this player's field
    public DrawnCard GetCardOnField(long matchid){

        foreach(card; FieldedCards){

            if(card.MatchCardNumber == matchid){

                return card;
            }
        }

        return null;
    }

    //! \returns A card that is owned by this player and either in hand
    //! or on the field (or banned)
    public DrawnCard GetOwnedCard(long matchid){

        auto card = GetCardInHand(matchid);

        if(card !is null)
            return card;

        card = GetCardOnField(matchid);

        return card;
    }

    //! Returns true if this player own's the card
    public bool OwnsCard(DrawnCard card) const{

        foreach(i; FieldedCards){

            if(i is card)
                return true;
        }

        foreach(i; CardsInHand){

            if(i is card)
                return true;
        }

        return false;
    }

    public void AddCard(DrawnCard card){
        
        CardsInHand ~= card;
    }

    //! Removes a card completely from this player if it is their card
    public void VoidCard(DrawnCard card){

        CardsInHand = CardsInHand.remove!(a => a is card);
        FieldedCards = FieldedCards.remove!(a => a is card);
    }
    
    unittest{

        auto cardtype = new Card;
        
        auto part = new this(new AI(AI_TYPE.EASY), 1);

        part.AddCard(cardtype.Draw(1, 1));
        part.AddCard(cardtype.Draw(2, 1));
        part.AddCard(cardtype.Draw(3, 1));

        assert(part.CardsInHand.length == 3);
        assert(part.GetCardInHand(2) is part.GetOwnedCard(2));
        assert(part.GetCardInHand(2) !is null);
        assert(part.GetCardInHand(4) is null);
        
        part.VoidCard(part.GetCardInHand(2));

        assert(part.CardsInHand.length == 2);
        assert(part.GetCardInHand(1) !is null);
        assert(part.GetCardInHand(3) !is null);
        assert(part.GetCardInHand(2) is null);

        part.VoidCard(part.GetCardInHand(1));
        part.VoidCard(part.GetCardInHand(3));

        assert(part.CardsInHand.length == 0);
    }
    

    public void ResetAttacked(){

        foreach(card; FieldedCards){

            card.AttackedThisTurn = false;
        }        
    }

    //! Informs the client that they need to provide a target
    public void RequireTarget(const PostEffect effect){

        // Just in case there is already a request, cancel that //
        if(PendingTarget !is null)
            TurnEnded();
        
        PendingTarget = new WaitingTarget(effect);

        // Send a message //
        SendMessage(new TargetRequiredMessage("Select Card to Apply: " ~ effect.ToString(),
                true, effect.CanTargetOwn,
                effect.CanTargetOpponent));
    }

    //! Cancels all target requests
    public void TurnEnded(){

        if(PendingTarget !is null){

            // Notify no longer valid //
            SendMessage(new TargetRequiredMessage(true));
            
            PendingTarget = null;
        }
    }

    //! Moves a card from hand array to field array
    public bool FieldCard(DrawnCard card){

        for(int i = 0; i < CardsInHand.length; ++i){

            if(CardsInHand[i] == card){
                
                CardsInHand = CardsInHand.remove(i);
                FieldedCards ~= card;
                card.Fielded = true;
                return true;
            }
        }

        return false;
    }

    public void RemovePossiblePost(long id){

        _PossiblePosts = _PossiblePosts.remove!(a => a.ID == id);
    }

    public void MarkPostAsPosted(const MatchPost post){

        assert(!PostedMessage);
        
        PostedMessage = true;

        _PossiblePosts = _PossiblePosts.remove!(a => a is post);
    }


    public @property DrawnCard[] AllCards(){

        return CardsInHand ~ FieldedCards;
    }
    
    // Gets card from index
    public DrawnCard GetCard(ulong index){

        return CardsInHand[index];
    }

    public DrawnCard GetFieldedCard(ulong index){

        return FieldedCards[index];
    }

    public @property ulong CountCardsInHand(){

        return CardsInHand.length;
    }

    public @property ulong CountCardsOnField(){

        return FieldedCards.length;
    }

    public @property ulong CountNonBannedCards(){

        ulong result = 0;

        foreach(card; FieldedCards){

            if(!card.Banned)
                ++result;
        }
        
        return result;
    }

    public @property bool IsConnected(){

        if(_Player is null)
            return false;

        return true;
    }

    public @property MatchPost Post(long id){

        foreach(post; _PossiblePosts){

            if(post.ID == id)
                return post;
        }
        
        return null;
    }

    // Cards in hand
    private DrawnCard[] CardsInHand;

    // Cards on the main battlefield
    private DrawnCard[] FieldedCards;

    private MatchPost[] _PossiblePosts;
    
    public BasePlayer _Player;

    public const bool IsAI = false;

    private int WinnerRatings = 0;

    
    private WaitingTarget PendingTarget = null;

    //! Waiting messages
    private MessageQueueItem[] MessageQueue;

    //! Delay accumulator used to handle MessageQueue
    private Duration DelayAccumulator;

    // 0 If this is player 0, 1 if player 2
    public int PlayerNumber;

    //! The amount of energy the player has, required to play cards
    public int Energy = 0;

    //! True if the player has posted a message this turn
    public bool PostedMessage = false;

    public @property bool HasPosted(){

        return PostedMessage;
    }

    public @property MatchPost[] PossiblePosts(){

        return _PossiblePosts;
    }

    public @property bool HasPendingTarget(){
        return PendingTarget !is null;
    }

    invariant{

        assert(PlayerNumber == 0 || PlayerNumber == 1);
    }

    unittest {

        auto card = new Card(1);
        auto drawn1 = card.Draw(0);
        auto drawn2 = card.Draw(1);
        auto drawn3 = card.Draw(2);

        auto part = new Participant(new AI(AI_TYPE.EASY), 0);

        part.AddCard(drawn1);

        assert(part.GetCardInHand(drawn2.MatchCardNumber) is null);

        part.AddCard(drawn2);

        assert(part.GetCardInHand(drawn2.MatchCardNumber) !is null);
        assert(part.GetCardInHand(drawn3.MatchCardNumber) is null);

        auto part2 = new Participant(new AI(AI_TYPE.EASY), 1);

        assert(part2.GetCardInHand(drawn3.MatchCardNumber) is null);

        part2.AddCard(drawn3);

        assert(part2.GetCardInHand(drawn1.MatchCardNumber) is null);
        assert(part2.GetCardInHand(drawn3.MatchCardNumber) !is null);
    }
}


//! Used to add delays to posts
class MessageQueueItem{

    this(IMessage message, Duration delay){

        Delay = delay;
        Message = message;
    }

    this(IMessage message){

        Message = message;
    }

    this(Duration delay){

        Delay = delay;
    }

    protected Duration Delay = 0.msecs;
    protected IMessage Message = null;
}


// test for HandleQueuedMessages
unittest{

    auto first = new SendMessageCollector;

    assert(first.Empty);

    auto part = new Participant(first);

    void Update(){

        part.HandleQueuedMessages(GAME_TICK_INTERVAL);
    }

    part.SendMessage(new HelloMessage("ply"));

    assert(!first.Empty);

    first.Clear();

    assert(first.Empty);

    part.SendDelay(GAME_TICK_INTERVAL);
    part.SendMessage(new HelloMessage("ply"));

    assert(first.Empty);
    assert(part.MessageQueue.length == 2);

    part.HandleQueuedMessages(GAME_TICK_INTERVAL);

    // Still empty after first delay
    assert(first.Empty);
    assert(part.DelayAccumulator == 0.msecs);
    assert(part.MessageQueue.length == 1);

    part.HandleQueuedMessages(GAME_TICK_INTERVAL);

    // Not empty after second delay
    assert(part.MessageQueue.length == 0);
    assert(!first.Empty);
    
}
