// Contains message objects for communicating with the client //
import config;
import connection;
import cards;
import match;
import player;
import threads;

import std.json;
import std.conv;
import std.container.array;
import std.exception;
import std.algorithm;
import core.exception;
import std.traits : hasUDA;
import std.typecons : tuple, Tuple;


enum MessageType {

    // Server kick player because of an error
    Error = 1,

    // First message. logs in the player.
    // Server responds with the same message confirming the name
    Hello = 120,
    
    // Client wants to play against someone
    StartRequest = 121,
    
    // Server accepts a match and the client will start receiving match events. Like DrawHand
    // and CardAction and TurnChange
    MatchStart = 122,
    
    // Once a match ends by win condition or forfeit or disconnect
    MatchEnd = 123,
    
    // Client or his opponent draws cards
    DrawHand = 124,

    // Client ends his turn
    TurnEndRequest = 125,

    // Server notifies client about changed turn
    TurnChange = 126,

    // Card does something, moves from hand to the playing field, attacks, etc.
    CardAction = 127,

    // Server sends a general chat message, nothing too important
    // UNUSED
    ChatText = 128,

    // Server tells the client the state of a match, if the state == ENDED the player should
    // move to the lobby
    MatchStatusUpdate = 129,

    // Send as a response to a client sent action,
    // tells the client whether the action succeeded or not
    ActionFinished = 130,

    // A card is revealed and the client can now see it
    CardReveal = 131,

    // A card is banned (killed) or unbanned
    CardBannedStatus = 132,

    // A card property (attack, defence points) has updated
    CardStatsUpdated = 133,

    // Server tells a client the currently online players
    ClientList = 134,

    // Server notifies a client about a new client or a disconnected client
    ClientStatus = 135,

    // Contains a single chat message either from the server to the client or
    // from the server broadcast to clients
    ChatMessage = 136,

    // Client challenges another to a match
    // Or cancels that challenge
    // Also used to accept the challenge if players challenge each other at the same time
    ClientChallengeOther = 137,

    // Server replies about whether the challenge failed or the other accepted it
    ChallengeStatus = 138,
    
    // Client is notified that it has been challenged to a match
    NotifyChallenged = 139,

    // Client needs to popup a message saying the serve ris going down
    ServerGoingDown = 140,

    // Client is told that the game has ended and should go back to main screen
    ClientExitMatch = 141,

    // Client wants to give up and lose a match
    ClientRequestDefeat = 142,

    // Energy of a player has updated
    EnergyUpdated = 143,

    // Server has selected a thread for the current match
    ThreadSelect = 144,

    // Server has given the player the option to make a post
    PostDrawn = 145,

    // Server informs a player that their possible post is no longer available
    PostDecayed = 146,

    // Winner ratings of a player has updated
    WinnerRatingsUpdated = 147,

    // When client needs to choose a target for a spell / post
    TargetRequired = 148,

    // A new post has been made
    NewPostDone = 149,

    // A player is now accepting or denying challenges
    ClientAllowChallenge = 150,

    // A card has been completely destroyed and should be removed from the game
    CardVoided = 151,

    // The client wants to discard an effect, if not allowed by the server the client
    // will be prompted for a new target
    DiscardCardEffect = 152,

    // Server tells the client current settings
    GameRuleSettings = 153,

    // Server has just applied a spell effect and the client should show an effect
    PlaySpellEffect = 154,

    // All of these actions work like this: client creates them, applies them locally and then
    // sends to the server for validation.
    // If the server rejects the action the client will undo it.
    // The client will also receive actions for their opponen't from the server,
    // those will never be undone

    // Playing a new card to the battlefield
    ActionPlayCard = 500,

    // Player attacks a target with a card
    ActionAttack = 501,

    // Player posts a post that the server has told the client about in a PostDrawn message
    ActionPost = 502,

    // Client has targeted something
    ActionTargetSelected = 503,

}

//! Main class for message types
class IMessage{

    public this(MessageType type){

        Type = type;
    }

    //! Serializes this message into JSON
    public abstract JSONValue Serialize();

    //! Parses and executes a JSON message. Throws if invalid
    public static void ExecuteMessage(const JSONValue message, Connection runon){

        auto idval = "id" in message;

        if(!idval)
            throw new MessageException("no id");

        const MessageType type = to!MessageType(idval.integer);
        
        switch(type){
            // Messages //
        case MessageType.Hello:
            return runon.ExecuteMessage(new HelloMessage(message));
        case MessageType.StartRequest:
            return runon.ExecuteMessage(new StartRequestMessage(message));
        case MessageType.TurnEndRequest:
            return runon.ExecuteMessage(new TurnEndRequestMessage(message));
        case MessageType.ChatMessage:
            return runon.ExecuteMessage(new ChatMessageMessage(message));
        case MessageType.ClientChallengeOther:
            return runon.ExecuteMessage(new ClientChallengeOtherMessage(message));
        case MessageType.ClientRequestDefeat:
            return runon.ExecuteMessage(new ClientRequestDefeatMessage(message));
        case MessageType.DiscardCardEffect:
            return runon.ExecuteMessage(new DiscardCardEffectMessage(message));
            // Actions //
        case MessageType.ActionPlayCard:
            return runon.ExecuteAction(new PlayCardAction(message));
        case MessageType.ActionAttack:
            return runon.ExecuteAction(new AttackAction(message));
        case MessageType.ActionPost:
            return runon.ExecuteAction(new PostAction(message));
        case MessageType.ActionTargetSelected:
            return runon.ExecuteAction(new TargetSelectedAction(message));
            
        default:
            throw new MessageException("Unknown type: " ~ to!string(type));
        }
    }

    @MessageMember("id") public MessageType Type;
}

//! Used as an UDA to automatically serialize all message members
struct MessageMember{
    
    this(immutable string name){
    
        Name = name;
    }
    
    public immutable string Name;
}

//! Used with hasUDA to determine if MessageMember is optional
struct Optional{

}

//! Base class for all messages to and from the client
class BaseMessage(MessageType TType) : IMessage{

    this(){

        super(TType);
    }
    
    //! Default serializes all members in a message derived class
    public static JSONValue DefaultSerialize(TDerived:BaseMessage)(TDerived obj){
        
        JSONValue result;
        
        foreach (memberName; __traits(allMembers, TDerived)) {

            foreach (attr; __traits(getAttributes, __traits(getMember, obj, memberName))) {

                static if(is (typeof(attr) == MessageMember)){
                
                    result[attr.Name] = __traits(getMember, obj, memberName);
                }
            }
        }
        
        return result;
    }

    //! Returns a value from json value if it exists and is correct type
    public static T Get(T:long)(in string name, const JSONValue message){

        auto val = name in message;
        
        if(!val)
            throw new MessageException("Message parse: missing " ~ name);

        return val.integer;
    }

    public static T Get(T)(in string name, const JSONValue message){

        static assert(false, "Get missing specification for type");
    }

    
    version(unittest){
        
        protected static void CheckMessageMemberValues(TDerived:BaseMessage)(
            TDerived first, TDerived second)
        {
        
            foreach (memberName; __traits(allMembers, TDerived)) {

                foreach (attr; __traits(getAttributes, __traits(getMember, first, memberName))) {

                    static if(is (typeof(attr) == MessageMember)){
                
                        assert(__traits(getMember, first, memberName) ==
                               __traits(getMember, second, memberName));
                    }
                }
            }
        }
    }
}

//! Base for all action messages
class BaseAction(MessageType TType) : BaseMessage!(TType) {

    public this(long number){

        SequenceNumber = number;
    }

    public this(const JSONValue message){

        SequenceNumber = Get!long("actionnumber", message);
    }

    //! Calls DoAction on a match object
    public abstract void Dispatch(Match match, Player ply);

    @MessageMember("actionnumber") public long SequenceNumber;
}

//! For access to a list of cards that have been sent to client
class SentCardCache {

    //! Returns false the first time an instance of Card is passed to this function and true
    //! after that
    //! Not synchronized between threads. As only a single match thread should be sending cards at
    //! once
    public bool IsAlreadySent(in Card card){

        immutable found = AlreadySentCardDefinitions.canFind(card);

        if(!found){

            AlreadySentCardDefinitions ~= card;
        }
        
        return found;
    }

    public @property ulong SentCount(){

        return AlreadySentCardDefinitions.length;
    }

    private const(Card)[] AlreadySentCardDefinitions;
    

    unittest{

        auto cache = new SentCardCache();
        auto card = new Card();
        auto card2 = new Card();

        assert(!cache.IsAlreadySent(card));
        assert(cache.IsAlreadySent(card));
        
        assert(!cache.IsAlreadySent(card2));
        assert(cache.IsAlreadySent(card2));
        
        assert(cache.IsAlreadySent(card));

        auto cache2 = new SentCardCache();
        assert(!cache2.IsAlreadySent(card));
        assert(cache2.IsAlreadySent(card));
        assert(cache.IsAlreadySent(card));
    }
}

enum string ImplementDefaultSerialize = q{

    public override JSONValue Serialize(){

        return super.DefaultSerialize(this);
    }
    
    version(unittest){
        
        private void CheckValues(typeof(this) other){

            super.CheckMessageMemberValues(this, other);
        }
    }
};

enum string ImplementInnerConstructor = q{

    foreach (memberName; __traits(allMembers, typeof(this))) {

        foreach (attr; __traits(getAttributes, __traits(getMember, this, memberName))) {

            static if(is (typeof(attr) == MessageMember)){

                auto val = attr.Name in message;

                if(!val){
                        
                    if(hasUDA!(__traits(getMember, this, memberName), Optional)){
                        // Skip as this is optional
                        break;
                        
                    } else {
                        
                        throw new MessageException("Message parse: missing " ~ attr.Name);
                    }
                }
                    
                static if(is (typeof(__traits(getMember, this, memberName)) == bool)){
                        
                    // Boolean handling //
                    __traits(getMember, this, memberName) = (val.type == JSON_TYPE.TRUE);
                    
                } else static if(is (typeof(__traits(getMember, this, memberName)) == string)){

                    if(val.type != JSON_TYPE.STRING)
                        throw new MessageException("Message member '" ~ attr.Name ~
                                                   "' is the wrong type " ~ to!string(val.type) ~
                                                   " != " ~ to!string(JSON_TYPE.STRING)
                                                   ~ " Message: " ~ to!string(message));
                    
                    __traits(getMember, this, memberName) = val.str;
                        
                } else {

                    // Assume it's a long value and needs to be an integer
                    if(val.type != JSON_TYPE.INTEGER)
                        throw new MessageException("Message member '" ~ attr.Name ~
                                                   "' is the wrong type " ~ to!string(val.type) ~
                                                   " != " ~ to!string(JSON_TYPE.INTEGER)
                                                   ~ " Message: " ~ to!string(message));
                    
                    __traits(getMember, this, memberName) = 
                    std.conv.to!(typeof(__traits(getMember, this, memberName)))(val.integer);
                }
            }
        }
    }
};

enum string ImplementDefaultMessage = q{

    mixin (ImplementDefaultSerialize);

    //! Default unserializes all members from a message derived class
    //! throws MessageException if required members are missing
    public this(const JSONValue message){


        mixin (ImplementInnerConstructor);
    }

};

enum string ImplementDefaultAction = q{

    mixin (ImplementDefaultSerialize);

    //! Default unserializes all members from a message derived class
    //! throws MessageException if required members are missing
    public this(const JSONValue message){

        super(message);
        
        mixin (ImplementInnerConstructor);
    }
    
    public override void Dispatch(Match match, Player ply){

        match.DoAction(ply, this);
    }
};

///////////// Message types /////////////

class HelloMessage : BaseMessage!(MessageType.Hello) {
    
    mixin (ImplementDefaultMessage);
    
    public this(in string name){

        Name = name;
    }

    @MessageMember("version") public string Version = ServerVersion;
    @MessageMember("name") public string Name;

    unittest{

        auto first = new typeof(this)("The chosen one");

        auto second = new typeof(this)(first.Serialize);
        
        first.CheckValues(second);

        assert(first.Serialize["id"].integer = MessageType.Hello);
    }
}

class ErrorMessage : BaseMessage!(MessageType.Error) {

    mixin (ImplementDefaultMessage);
    
    public this(in string error){

        Error = error;
    }

    @MessageMember("error") public string Error;

    unittest{

        auto first = new typeof(this)("First error");

        auto second = new typeof(this)(first.Serialize);
        
        first.CheckValues(second);
    }
}

class StartRequestMessage : BaseMessage!(MessageType.StartRequest) {

    mixin (ImplementDefaultMessage);
    
    public this(in string opponent, in string deck = "default")
        in{
            assert(opponent.length > 0);
            assert(deck.length > 0);
        }
    body{

        Opponent = opponent;
        Deck = deck;
    }

    //! Name of opponent or an AI
    @MessageMember("opponent") public string Opponent;
    //! When custom decks are defined this will contain that    
    @MessageMember("deck") @Optional public string Deck = "default";

    unittest{

        auto first = new typeof(this)("Loser", "best deck");

        auto second = new typeof(this)(first.Serialize);
        
        first.CheckValues(second);
    }
}

class MatchStartMessage : BaseMessage!(MessageType.MatchStart) {

    mixin (ImplementDefaultMessage);

    public this(in string opponent)
        in{
            assert(opponent.length > 0);
        }
    body{

        Opponent = opponent;
    }

    //! Name of opponent or an AI
    @MessageMember("opponent") public string Opponent;

    unittest{

        auto first = new typeof(this)("Beastmaster");

        auto second = new typeof(this)(first.Serialize);
        
        first.CheckValues(second);
    }
}

class MatchEndMessage : BaseMessage!(MessageType.MatchEnd) {

    mixin (ImplementDefaultMessage);

    public this(in string resultmessage, in bool wonbytarget, in int score){

        ResultStr = resultmessage;
        Won = wonbytarget;
        Score = score;
    }

    @MessageMember("result") public string ResultStr;
    @MessageMember("won") public bool Won;
    @MessageMember("score") public int Score;

    unittest{

        auto first = new typeof(this)("Beastmaster won", true, 42);

        auto second = new typeof(this)(first.Serialize);
        
        first.CheckValues(second);
    }
}

class DrawHandMessage : BaseMessage!(MessageType.DrawHand) {

    // Cannot be received...

    // Create to send message about cards entering play
    // forconnection is used to cache sent cards
    public this(in int count, in bool opponentscards, immutable DrawnCard[] cards,
                SentCardCache alreadysent)
        in{
            assert(count > 0);
            assert(cards.length == count);
        }
    body{

        Count = count;
        Opponents = opponentscards;
        Cards = cards;

        // Might be null //
        CardCache = alreadysent;
    }

    public override JSONValue Serialize(){
        
        JSONValue result = super.DefaultSerialize(this);

        JSONValue[] cardarray;

        foreach(card; Cards){

            // Hides type if Opponents is true
            cardarray ~= card.Serialize(CardCache, Opponents);
        }

        result["cards"] = cardarray;

        return result;
    }

    // Number of cards drawn. Must be over 1
    @MessageMember("count") public int Count;
    
    // True if opponents cards. And they are hidden
    @MessageMember("opponents") public bool Opponents;

    // The cards. Must be valid even if Opponents is true to identify the match ids of the cards
    // in opponents hand
    public const immutable(DrawnCard)[] Cards;

    //! Used to send each card definition only once
    private SentCardCache CardCache;
    
    unittest{

        auto cache = new SentCardCache();
        
        auto t1 = new Card();

        immutable card1 = cast(immutable)t1.Draw();

        immutable cards = [ card1 ];
        const opponents = false;
        
        auto first = new DrawHandMessage(to!int(cards.length), opponents, cards, cache);

        assert(first.Type == MessageType.DrawHand);
        assert(first.Count == cards.length);

        auto serialized = first.Serialize;

        assert(serialized["id"].integer == MessageType.DrawHand);
        assert(serialized["count"].integer == first.Count);
        assert((serialized["opponents"].type == JSON_TYPE.TRUE) == first.Opponents);
        assert(serialized["cards"][0]["id"].integer == card1.CardID);
    }
}
    
//! Client ends his turn
class TurnEndRequestMessage : BaseMessage!(MessageType.TurnEndRequest) {

    mixin (ImplementDefaultMessage);

    public this(){

    }
}

//! Server notifies client about changed turn
class TurnChangeMessage : BaseMessage!(MessageType.TurnChange) {

    mixin (ImplementDefaultMessage);

    public this(in MATCH_STATE state, bool receiverisfirstplayer, in string message = ""){

        if((state == MATCH_STATE.TURN_PLY1 && receiverisfirstplayer) ||
           // 2nd player's turn (his message)
           (state == MATCH_STATE.TURN_PLY2 && !receiverisfirstplayer))
        {
            OwnTurn = true;
            
        } else {

            OwnTurn = false;
        }

        Message = message;
        WhoseTurn = state;
    }

    @MessageMember("whoseturn") public MATCH_STATE WhoseTurn;
    @MessageMember("message") @Optional public string Message = "";
    @MessageMember("ownturn") public bool OwnTurn;

    unittest{

        auto first = new TurnChangeMessage(MATCH_STATE.TURN_PLY1, true, "Hi there");

        assert(first.OwnTurn == true);
        assert(first.Message == "Hi there");
        assert(first.WhoseTurn == MATCH_STATE.TURN_PLY1);

        auto second = new TurnChangeMessage(first.Serialize);

        assert(first.WhoseTurn == second.WhoseTurn);
        assert(first.Message == second.Message);
        assert(first.OwnTurn == second.OwnTurn);

        assert(first.Type == MessageType.TurnChange);
        assert(second.Type == MessageType.TurnChange);

        
        first = new TurnChangeMessage(MATCH_STATE.TURN_PLY1, true);

        second = new TurnChangeMessage(first.Serialize);
        
        assert(first.WhoseTurn == second.WhoseTurn);
        assert(first.Message == second.Message);
        assert(first.OwnTurn == second.OwnTurn);

        first = new TurnChangeMessage(MATCH_STATE.TURN_PLY2, true);

        assert(first.OwnTurn != true);

        first = new TurnChangeMessage(MATCH_STATE.TURN_PLY2, false);

        assert(first.OwnTurn == true);
    }
}

//! Action status from the server to the client
class ActionFinishedMessage : BaseMessage!(MessageType.ActionFinished) {

    mixin (ImplementDefaultMessage);

    public this(long number, bool succeeded){

        Succeeded = succeeded;
        ActionNumber = number;
    }

    @MessageMember("succeeded") public bool Succeeded;
    @MessageMember("actionnumber") public long ActionNumber;

    unittest{

        auto first = new typeof(this)(0, true);

        auto second = new typeof(this)(first.Serialize);
        
        first.CheckValues(second);
    }
}

//! Server tells the client of the type of a hidden card
//! server can't receive these
class CardRevealMessage : BaseMessage!(MessageType.CardReveal) {

    public this(const DrawnCard card, SentCardCache alreadysent){

        RevealedCard = card;
        CardCache = alreadysent;
    }
    
    public override JSONValue Serialize(){
        
        JSONValue result = super.DefaultSerialize(this);
        result["card"] = RevealedCard.Serialize(CardCache, false);

        return result;
    }

    public const DrawnCard RevealedCard;
    private SentCardCache CardCache;

    unittest{

        auto cache = new SentCardCache();
        
        auto t1 = new Card();

        auto card1 = t1.Draw(1);

        auto first = new CardRevealMessage(card1, cache);

        assert(first.Type == MessageType.CardReveal);

        auto serialized = first.Serialize;

        assert(serialized["id"].integer == MessageType.CardReveal);
        assert(serialized["card"]["id"].integer == card1.CardID);
        assert(serialized["card"]["matchcard"].integer == card1.CardID);
        assert(serialized["card"]["full"].type == JSON_TYPE.TRUE);
    }
}


class CardBannedStatusMessage : BaseMessage!(MessageType.CardBannedStatus) {

    mixin (ImplementDefaultMessage);

    public this(long cardmatchid, int bannedstatus, bool rescuedbyclient){

        MatchID = cardmatchid;
        Status = bannedstatus;
        Rescuer = rescuedbyclient;
    }

    @MessageMember("matchid") public long MatchID;
    @MessageMember("status") public int Status;
    @MessageMember("rescuer") public bool Rescuer;

    unittest{

        auto first = new typeof(this)(4, 1, true);

        auto second = new typeof(this)(first.Serialize);
        
        first.CheckValues(second);
    }
}

class CardStatsUpdatedMessage : BaseMessage!(MessageType.CardStatsUpdated){

    mixin (ImplementDefaultMessage);

    public this(long cardmatchid, int attack, int defence, int cost){

        MatchID = cardmatchid;
        Attack = attack;
        Defence = defence;
        Cost = cost;
    }

    public this(DrawnCard card){

        MatchID = card.MatchCardNumber;
        Attack = card.InstanceAttack; 
        Defence = card.InstanceDefence;
        Cost = card.InstanceCost; 
    }

    @MessageMember("matchid") public long MatchID;
    @MessageMember("attack") public int Attack;
    @MessageMember("defence") public int Defence;
    @MessageMember("cost") public int Cost;

    unittest{

        auto first = new typeof(this)(12, 6, 3, 2);

        auto second = new typeof(this)(first.Serialize);
        
        first.CheckValues(second);
    }
}


class ClientListMessage : BaseMessage!(MessageType.ClientList){

    public this(in Tuple!(string, bool)[] players){

        Players = players;
    }

    public override JSONValue Serialize(){
        
        JSONValue result = super.DefaultSerialize(this);

        JSONValue[] array;

        foreach(ply; Players){

            JSONValue jj = [ "n": ply[0] ];
            jj["c"] = ply[1];
            
            array ~= jj;
        }
        
        result["players"] = array;

        return result;
    }

    public const(Tuple!(string, bool))[] Players;

}

class ClientStatusMessage : BaseMessage!(MessageType.ClientStatus){

    mixin (ImplementDefaultMessage);

    public this(in string player, bool online){

        Player = player;
        Online = online;
    }

    @MessageMember("player") public string Player;
    @MessageMember("online") public bool Online;

    unittest{

        auto first = new typeof(this)("xxButchererxxxxxx", true);

        auto second = new typeof(this)(first.Serialize);
        
        first.CheckValues(second);
    }
}

class ChatMessageMessage : BaseMessage!(MessageType.ChatMessage){

    mixin (ImplementDefaultMessage);

    public this(in string serverbroadcast){

        Message = serverbroadcast;
    }

    public this(in string sendername, in string message){

        Sender = sendername;
        Message = message;
    }

    @MessageMember("sender") @Optional public string Sender = "";
    @MessageMember("message") public string Message;

    unittest{

        auto first = new typeof(this)("Boss", "Hi to all");

        auto second = new typeof(this)(first.Serialize);
        
        first.CheckValues(second);

        JSONValue testdata;
        testdata["id"] = cast(int)first.Type;
        immutable string teststring = "Console says hi";
        testdata["message"] = teststring;
        
        auto third = new typeof(this)(testdata);

        assert(third.Sender == "");
        assert(third.Message == teststring);
    }
}

class ClientChallengeOtherMessage : BaseMessage!(MessageType.ClientChallengeOther){

    mixin (ImplementDefaultMessage);

    public this(in string opponent, in bool stillvalid){

        Opponent = opponent;
        Status = stillvalid;
    }

    @MessageMember("opponent") public string Opponent;
    @MessageMember("status") public bool Status;

    unittest{

        auto first = new typeof(this)("Boss", true);

        auto second = new typeof(this)(first.Serialize);
        
        first.CheckValues(second);
    }
}

class ChallengeStatusMessage : BaseMessage!(MessageType.ChallengeStatus){

    mixin (ImplementDefaultMessage);

    public this(in string opponent, in bool succeeded, in string message)
        in{
            if(!succeeded)
                assert(message.length > 0);
            else
                assert(message.length == 0);
        }
    body{

        Opponent = opponent;
        Status = succeeded;
        Message = message;
    }

    @MessageMember("opponent") public string Opponent;
    @MessageMember("status") public bool Status;
    @MessageMember("message") @Optional public string Message;

    unittest{

        auto first = new typeof(this)("Boss", true, "");

        auto second = new typeof(this)(first.Serialize);
        
        first.CheckValues(second);
    }
}

class ServerGoingDownMessage : BaseMessage!(MessageType.ServerGoingDown){

    mixin (ImplementDefaultMessage);

    public this(in string message){

        Message = message;
    }

    @MessageMember("message") public string Message;

    unittest{

        auto first = new typeof(this)("The server is toast!");

        auto second = new typeof(this)(first.Serialize);
        
        first.CheckValues(second);
    }
}
    
class NotifyChallengedMessage : BaseMessage!(MessageType.NotifyChallenged){

    mixin (ImplementDefaultMessage);

    public this(in string challengedby, in bool valid){

        Challenger = challengedby;
        Valid = valid;
    }

    @MessageMember("challenger") public string Challenger;
    @MessageMember("valid") public bool Valid;

    unittest{

        auto first = new typeof(this)("Boss", true);

        auto second = new typeof(this)(first.Serialize);
        
        first.CheckValues(second);
    }
}

class ClientExitMatchMessage : BaseMessage!(MessageType.ClientExitMatch){

    mixin (ImplementDefaultMessage);

    public this(){

    }

    @MessageMember("challenger") public string Challenger;
}

class ClientRequestDefeatMessage : BaseMessage!(MessageType.ClientRequestDefeat){

    mixin (ImplementDefaultMessage);

    public this(){

    }
}

class EnergyUpdatedMessage : BaseMessage!(MessageType.EnergyUpdated){

    mixin (ImplementDefaultMessage);

    public this(in bool own, in int energy){

        Own = own;
        Energy = energy;
    }

    @MessageMember("own") public bool Own;
    @MessageMember("energy") public int Energy;

    unittest{

        auto first = new typeof(this)(true, 24);

        auto second = new typeof(this)(first.Serialize);
        
        first.CheckValues(second);
    }
}

class ThreadSelectMessage : BaseMessage!(MessageType.ThreadSelect){

    mixin (ImplementDefaultMessage);

    public this(in Thread thread){

        Title = thread.Title;
        Link = thread.Link;
    }

    @MessageMember("title") public string Title;
    @MessageMember("link") public string Link;
    
    unittest{

        auto first = new typeof(this)(new Thread([new Post(1)], "A whole new thread", "nowhere"));

        auto second = new typeof(this)(first.Serialize);
        
        first.CheckValues(second);
    }
}

class PostDrawnMessage : BaseMessage!(MessageType.PostDrawn){

    mixin (ImplementDefaultMessage);

    public this(in MatchPost post){

        Message = post.PostData.Description;
        Effect = post.PostData.Effect.ToString;
        MatchID = post.ID;
        Decay = post.PostData.Decay;
    }

    @MessageMember("message") public string Message;
    @MessageMember("effect") public string Effect;
    @MessageMember("matchid") public long MatchID;
    @MessageMember("decay") public long Decay;
    
    unittest{

        auto first = new typeof(this)(new MatchPost(new Post(1), 1));

        auto second = new typeof(this)(first.Serialize);
        
        first.CheckValues(second);
    }
}

class PostDecayedMessage : BaseMessage!(MessageType.PostDecayed){

    mixin (ImplementDefaultMessage);

    public this(in MatchPost post){

        MatchID = post.ID;
    }

    @MessageMember("matchid") public long MatchID;
    
    unittest{

        auto first = new typeof(this)(new MatchPost(new Post(1), 1));

        auto second = new typeof(this)(first.Serialize);
        
        first.CheckValues(second);
    }    
}

class WinnerRatingsUpdatedMessage : BaseMessage!(MessageType.WinnerRatingsUpdated){

    mixin (ImplementDefaultMessage);

    public this(in bool own, in int winnerx){

        Own = own;
        WinnerX = winnerx;
    }

    @MessageMember("own") public bool Own;
    @MessageMember("winner") public int WinnerX;

    unittest{

        auto first = new typeof(this)(true, 24);

        auto second = new typeof(this)(first.Serialize);
        
        first.CheckValues(second);
    }
}

class TargetRequiredMessage : BaseMessage!(MessageType.TargetRequired){

    mixin (ImplementDefaultMessage);

    //! Cancels the currently active target request
    public this(in bool canceled)
        in{
            assert(canceled == true);
        }
    body{

        Canceled = true;
    }

    public this(in string requestmessage, in bool iscard, in bool canbeown,
        in bool canbeopponents)
    {

        Message = requestmessage;
        IsCard = iscard;
        CanTargetOwn = canbeown;
        CanTargetOpponent = canbeopponents;
    }

    @MessageMember("message") public string Message;
    @MessageMember("iscard") public bool IsCard;
    @MessageMember("canbeown") public bool CanTargetOwn;
    @MessageMember("canbeopponents") public bool CanTargetOpponent;
    //! If set to true the request has been canceled
    @MessageMember("canceled") @Optional public bool Canceled = false;

    unittest{

        auto first = new typeof(this)("select card to kill", true, false, true);

        auto second = new typeof(this)(first.Serialize);
        
        first.CheckValues(second);
    }
}

class NewPostDoneMessage : BaseMessage!(MessageType.NewPostDone){

    mixin (ImplementDefaultMessage);

    public this(in MatchPost post){

        Message = post.PostData.Text;
        Effect = post.PostData.Effect.ToString;
        Poster = post.PostData.Poster;
        Link = post.PostData.Link;
    }

    @MessageMember("message") public string Message;
    @MessageMember("effect") public string Effect;
    @MessageMember("poster") public string Poster;
    @MessageMember("link") public string Link;
    
    unittest{

        auto first = new typeof(this)(new MatchPost(new Post(1), 1));

        auto second = new typeof(this)(first.Serialize);
        
        first.CheckValues(second);
    }
}

class ClientAllowChallengeMessage : BaseMessage!(MessageType.ClientAllowChallenge){
    mixin (ImplementDefaultMessage);

    public this(in string name, bool challenge){

        Player = name;
        Challenge = challenge;
    }

    @MessageMember("player") public string Player;
    @MessageMember("challenge") public bool Challenge;
    
    unittest{

        auto first = new typeof(this)("name", true);

        auto second = new typeof(this)(first.Serialize);
        
        first.CheckValues(second);
    }
}

class CardVoidedMessage : BaseMessage!(MessageType.CardVoided) {

    mixin (ImplementDefaultMessage);

    public this(long cardmatchid){

        MatchID = cardmatchid;
    }

    @MessageMember("matchid") public long MatchID;
    
    unittest{

        auto first = new typeof(this)(2634734);

        auto second = new typeof(this)(first.Serialize);
        
        first.CheckValues(second);
    }
}

class DiscardCardEffectMessage : BaseMessage!(MessageType.DiscardCardEffect) {

    mixin (ImplementDefaultMessage);

    public this(){
    }
}

class GameRuleSettingsMessage : BaseMessage!(MessageType.GameRuleSettings) {

    mixin (ImplementDefaultMessage);

    public this(long winnerstovictory){

        MaxWinnerRatings = winnerstovictory;
    }

    @MessageMember("maxwinnerratings") public long MaxWinnerRatings;
    
    unittest{

        auto first = new typeof(this)(244);

        auto second = new typeof(this)(first.Serialize);
        
        first.CheckValues(second);
    }
}

class PlaySpellEffectMessage : BaseMessage!(MessageType.PlaySpellEffect) {

    mixin (ImplementDefaultMessage);

    public this(in string effect, long matchcardnumber, in string origin = "center"){

        EffectName = effect;
        MatchCardNumber = matchcardnumber;
        Origin = origin;
    }

    @MessageMember("effectname") public string EffectName;
    @MessageMember("matchcard") public long MatchCardNumber;
    @MessageMember("origin") public string Origin;
    
    unittest{

        auto first = new typeof(this)("flame", 256, "center");

        auto second = new typeof(this)(first.Serialize);
        
        first.CheckValues(second);
    }
}


///////////// All the action messages /////////////


//! Player plays a card
class PlayCardAction : BaseAction!(MessageType.ActionPlayCard) {

    mixin (ImplementDefaultAction);

    public this(long matchcardnumber, long cardtype, long sequencenumber){

        super(sequencenumber);

        MatchCardNumber = matchcardnumber;
        CardType = cardtype;
    }

    @MessageMember("matchcard") public long MatchCardNumber;
    @MessageMember("cardid") public long CardType;

    unittest{

        auto first = new typeof(this)(3, 1, 0);

        auto second = new typeof(this)(first.Serialize);
        
        first.CheckValues(second);
    }
}

class AttackAction : BaseAction!(MessageType.ActionAttack) {

    mixin (ImplementDefaultAction);

    public this(long attacker, long target, long sequencenumber){

        super(sequencenumber);

        Attacker = attacker;
        Target = target;
    }

    //! MatchID of the attacking card
    @MessageMember("attacker") public long Attacker;
    //! MatchID of the targeted card
    @MessageMember("target") public long Target;

    unittest{

        auto first = new typeof(this)(3, 1, 0);

        auto second = new typeof(this)(first.Serialize);
        
        first.CheckValues(second);
    }
}

class PostAction : BaseAction!(MessageType.ActionPost) {

    mixin (ImplementDefaultAction);

    public this(long post, long sequencenumber){

        super(sequencenumber);

        MatchPostID = post;
    }

    //! ID of the post that the client wants to make
    @MessageMember("matchid") public long MatchPostID;

    unittest{

        auto first = new typeof(this)(5, 2);

        auto second = new typeof(this)(first.Serialize);
        
        first.CheckValues(second);
    }
}

class TargetSelectedAction : BaseAction!(MessageType.ActionTargetSelected) {

    mixin (ImplementDefaultAction);

    public this(in string type, in long id, long sequencenumber)
        in{
            assert(type == "card");
        }
    body{

        super(sequencenumber);

        TargetType = type;
        TargetID = id;
    }

    //! Must be one of: card
    @MessageMember("targettype") public string TargetType;
    //! ID number of the target, together with TargetType specifies what was targeted
    @MessageMember("targetid") public long TargetID;

    unittest{

        auto first = new typeof(this)("card", 24, 1);

        auto second = new typeof(this)(first.Serialize);
        
        first.CheckValues(second);
    }
}

