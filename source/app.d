import network;
import games;

import vibe.d;

import core.time;
import std.conv : to;
import std.stdio;

import core.thread;
import std.concurrency;
import std.socket;
import std.file : remove;

// For better stack traces
import etc.linux.memoryerror;

static immutable Version = "0.1.0";

//! Does a graceful exit notifying all players and saving active matches
public void GracefulExit(string reason){

    logInfo("Gracefully exiting for reason: " ~ reason);
    
    exitEventLoop();

    // Static destructor will do saving
}

static shared GameManager GamesHost = null;

static TaskWrapper CmdPumpTask = null;

shared static this(){

    static if (is(typeof(registerMemoryErrorHandler)))
        registerMemoryErrorHandler();

    // Initialized here to read file at runtime //
    GamesHost = cast(shared)(new GameManager);

    RCONRun = true;
    CmdPumpTask = new TaskWrapper(runTask({ LocalCommandPump; }));
    //CmdPumpThread = std.concurrency.spawn(&LocalCommandPump);

    setLogFile("server_log.txt", LogLevel.info);

    debug{
        setLogLevel(LogLevel.debug_);
    }
    
	auto router = new URLRouter;
	router.get("/ws", handleWebSockets(&handleWebSocketConnection));
    
	auto settings = new HTTPServerSettings;
	settings.port = 8080;
	settings.bindAddresses = ["::1", "127.0.0.1"];
	listenHTTP(settings, router);
}

shared static ~this(){

    //RCONRun = false;
    CmdPumpTask.task.sendCompat(42);
    //std.concurrency.send(CmdPumpThread, 25);

    // Save state //
    logInfo("TODO: actually save active matches");
    //exitEventLoop();
}

void handleWebSocketConnection(scope WebSocket socket){
    
	logInfo("New client connected");

    scope connection = Network.WrapSocket(socket, cast(GameManager)GamesHost);

    try{
        
        connection.RunConnection();
        
    } catch(Exception e){

        logError("Uncaught exception from RunConnection: " ~e.msg);
    }

    logInfo("Client disconnected.");
    
    connection.Close();
}

//static Tid CmdPumpThread;

static immutable SocketName = "./rcon/localcommands.sock";

static shared bool RCONRun = false;

private void LocalCommandPump(){

    std.socket.Socket server = new Socket(AddressFamily.UNIX,
                               SocketType.STREAM,
                               ProtocolType.IP);
    try{
        remove(SocketName);
    } catch(Exception){
    }
    
    server.bind(new UnixAddress(SocketName));
    server.blocking = false;
    server.listen(5);

    if(!server.isAlive()){

        logError("Error opening RCON socket");
        return;
    }

    logDebug("RCON socket opened at: " ~ SocketName);
    
    scope(exit) {
        server.shutdown(SocketShutdown.BOTH);
        server.close();
    }
    
    while(RCONRun){

        try{

            auto client = server.accept();
            client.blocking = true;

            logDebug("Received rcon connection");

            std.concurrency.spawn(&RCONConnectionHandler, cast(shared)client);
            
        } catch(SocketAcceptException){

            // No connections //
        }

        receiveTimeoutCompat(dur!"msecs"(250),
            (int i) { logTrace("Got:" ~ to!string(i)); RCONRun = false; });
    }


    logDebug("Quitting local listener");
}

private void RCONConnectionHandler(shared Socket socket){

    auto client = cast(Socket)socket;

    // Welcome message //
    client.send("\nFPCardGame Server(" ~ Version ~ ")- RCON Interface. " ~
        "\nReady for commands\n");

    scope(exit){

        client.send("Closing connection.\n");
        client.shutdown(SocketShutdown.BOTH);
        client.close();
        writeln("Closed rcon handler thread command");
    }

    writeln("Receiving RCON data...");

    char[1024] buffer;
    immutable bytes = client.receive(buffer);
    immutable message = to!string(buffer[0 .. bytes]).stripRight;

    if(bytes >= 1024){

        client.send("Warning: command was too long. It was truncated to: " ~ message);
    }

    writeln("Received " ~ to!string(bytes) ~ " bytes of data");
    client.send("\nCommand received. Processing.\n");

    if(!message.empty){

        if(message.length > 0){

            GamesHost.ProcessLocalCommand(message);
        }

        writeln("Command is now queued");
    }

    client.send("It is done\n");
}


