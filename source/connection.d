import config;
import network;
import messages;
import player;
import cards;
import games;
import match;
import utility;

import vibe.d;
import std.json;
import std.algorithm;

class SocketWrapper {

    public this(WebSocket socket){

        Socket = socket;
    }

    public void Break(){

        Socket = null;
    }

    //! \returns True if the connection isn't closed
    public bool SendMessage(IMessage message){

        return SendMessage(message.Serialize);
    }

    //! \returns True if the connection isn't closed
    public bool SendMessage(const JSONValue message){

        if(Socket is null)
            return false;

        string asstr = message.toString();

        debug{
            logDebug("Sending message: " ~ asstr);
        }

        if(!Socket.connected)
            return false;
        
        Socket.send(asstr);
        return true;
    }

    private WebSocket Socket = null;
}

class Connection{

    version(unittest){
        // Dummy for unittest
        public this(){

        }
    }

    public this(WebSocket socket, GameManager host){

        Socket = new SocketWrapper(socket);
        _WebSocket = socket;
        GamesHost = host;
    }

    public ~this(){
        
    }

    public void Close(){

        Socket.Break;

        if(ConnectionsPlayer !is null){

            logInfo("Player disconnected!, " ~ConnectionsPlayer.Name);

            GamesHost.LogOut(ConnectionsPlayer);

            ConnectionsPlayer = null;
        }
    }
    
    public void RunConnection(){

        scope(exit) Socket.Break();

        while (_WebSocket.waitForData()) {

            string message;
        
            try{
                message = _WebSocket.receiveText();
            }
            catch(WebSocketException e){

                logError("Client caused an exception in a WebSocket: " ~ e.msg);
                continue;
            }

            debug{
                logDebug("Got message: " ~ message);
            }

            JSONValue messageobj;

            try{
                messageobj = parseJSON(message);
            } catch(JSONException e){

                logError("Client sent an invalid JSON structure: " ~ e.msg);
                continue;
            }
            

            try{
                
                HandleMessage(messageobj);
                
            } catch(MessageException e){

                logError("Client sent an invalid message: " ~ e.msg);
                
            } catch(JSONException e){

                logError("Client sent a message containing wrong types: " ~ e.msg);
                logError("Error trace: " ~ to!string(e.info));
            }
        }
    }

    private void HandleMessage(const JSONValue message){

        IMessage.ExecuteMessage(message, this);
    }

    public void SendMessage(IMessage message){

        Socket.SendMessage(message);
    }

    //! Executes non-specialized message
    public void ExecuteMessage(IMessage parsed){

        logError("Default action for message: " ~ to!string(parsed.Type));

    }

    // Reply to hello message
    public void ExecuteMessage(HelloMessage hello){

        // Player logs in //
        try{
            ConnectionsPlayer = new Player(hello.Name, Socket);

        } catch(Exception e){

            SendMessage(new ErrorMessage("Login failed: " ~ e.msg));
            _WebSocket.close();
            return;
        }

        try{
            // Notify games //
            GamesHost.RegisterPlayer(ConnectionsPlayer);
            
        } catch(Exception e){

            // Name is taken / login failed //
            SendMessage(new ErrorMessage("Name taken / login failed: " ~ e.msg));
            _WebSocket.close();
            return;
        }

        // Gameshost sends the hello message if it succeeded //
        
    }

    // Start match
    public void ExecuteMessage(StartRequestMessage start){

        if(ConnectionsPlayer is null)
            return;

        logInfo("New match started or challenge accepted");

        const aitype = Player.GetAIFromString(start.Opponent);

        if(start.Opponent != AIEasy || aitype != AI_TYPE.EASY){

            // Unknown opponent //
            SendMessage(new MatchEndMessage("unknown opponent", false, 0));
            return;
        }

        GamesHost.StartAIMatch(ConnectionsPlayer, aitype);
        
        SendMessage(new MatchStartMessage(start.Opponent));
    }

    // End player turn //
    public void ExecuteMessage(TurnEndRequestMessage message){

        if(ConnectionsPlayer is null || ConnectionsPlayer.PartInMatch is null)
            return;

        ConnectionsPlayer.PartInMatch.EndTurnFor(ConnectionsPlayer);
    }

    // Player discards an effect
    public void ExecuteMessage(DiscardCardEffectMessage message){
        
        if(ConnectionsPlayer is null || ConnectionsPlayer.PartInMatch is null)
            return;

        ConnectionsPlayer.PartInMatch.PlayerTryDiscardEffect(ConnectionsPlayer);
    }

    // Player sends a chat message
    public void ExecuteMessage(ChatMessageMessage message){

        if(ConnectionsPlayer is null || message.Message.length > 400)
            return;

        message.Sender = ConnectionsPlayer.Name.RemoveTags;
        message.Message = message.Message.RemoveTags;

        try{
            GamesHost.Chat(ConnectionsPlayer, message);
        } catch(Exception){

            // Message was too long or something
        }
    }

    // Player wants to play with a nother
    public void ExecuteMessage(ClientChallengeOtherMessage message){

        if(ConnectionsPlayer is null)
            return;

        if(!message.Status){

            // Canceled challenge or denied a previous challenge //
            GamesHost.ChallengeCancelled(ConnectionsPlayer, message.Opponent);
            return;
        }

        Player target = GamesHost.GetPlayer(message.Opponent);

        if(target is null){

            SendMessage(new ChallengeStatusMessage(message.Opponent, false,
                                                          "Player is not online"));
            return;
        }

        logInfo(ConnectionsPlayer.Name ~ " challenged " ~ target.Name);

        // This function will send the status messages
        GamesHost.ChallengePlayer(ConnectionsPlayer, target);
    }

    // Player wants to give up
    public void ExecuteMessage(ClientRequestDefeatMessage message){

        if(ConnectionsPlayer is null || ConnectionsPlayer.PartInMatch is null)
            return;
        
        ConnectionsPlayer.PartInMatch.GiveUp(ConnectionsPlayer);
    }

    //! All actions go through this one
    public void ExecuteAction(T)(T action){

        if(ConnectionsPlayer is null || ConnectionsPlayer.PartInMatch is null)
            return;

        action.Dispatch(ConnectionsPlayer.PartInMatch, ConnectionsPlayer);
    }

    private Player ConnectionsPlayer = null;
    private SocketWrapper Socket;
    private WebSocket _WebSocket = null;
    private GameManager GamesHost;

    invariant{

        assert(Socket !is null);
    }
}
